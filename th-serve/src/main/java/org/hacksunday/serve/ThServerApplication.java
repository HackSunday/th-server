package org.hacksunday.serve;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "org.hacksunday.serve.mapper")
public class ThServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThServerApplication.class, args);
    }

}
