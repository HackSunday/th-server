package org.hacksunday.serve.vo;

import lombok.Getter;
import lombok.Setter;
import org.hacksunday.lib.GrpcUserAccount;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class TexasPlayer {

    private int userId;

    private String userName;

    // 当前玩家的账户信息
    private GrpcUserAccount userAccount;

    // 当前手牌
    private List<Card> cards = new ArrayList<>();

    // 投入的筹码
    private int putinChips;

    // 剩余筹码
    private int remainChips;

    // 玩家当前状态 弃牌、all in
    private PlayingStatus playingStatus = PlayingStatus.UNPREPARED;

    public boolean notFold() {
        return !playingStatus.equals(PlayingStatus.FOLD);
    }

    public boolean isAllIn() {
        return playingStatus.equals(PlayingStatus.ALL_IN);
    }

    public void addRemainChips(int add) {
        remainChips += add;
    }
}
