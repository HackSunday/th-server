package org.hacksunday.serve.vo;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TexasGameConfig {

    /**
     * 每回合超时时间
     */
    private int timeoutSeconds = 30;

    private int totalChairCount = 6;

    /**
     * 小盲大小
     */
    private int smallBlind = 10;
}
