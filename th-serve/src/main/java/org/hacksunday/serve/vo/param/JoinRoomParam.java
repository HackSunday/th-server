package org.hacksunday.serve.vo.param;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class JoinRoomParam {
    int roomId;
}
