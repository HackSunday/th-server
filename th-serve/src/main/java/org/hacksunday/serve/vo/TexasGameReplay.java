package org.hacksunday.serve.vo;

import org.hacksunday.lib.GrpcGameCommand;
import org.hacksunday.serve.business.TexasGame;

import java.util.LinkedList;
import java.util.Map;

/**
 * 记录游戏录像
 * 每一次的操作
 * 每一次的快照
 * fixme 为了实现方便使用了快照的方式，后续为了节约存储，应当只记录command实时演算，而无需保留快照
 */
public class TexasGameReplay {

    // steps line
    private LinkedList<Integer> steps;

    // stepIndex,command
    private Map<Integer, GrpcGameCommand> commandMap;

    // stepIndex,TexasGameSnapshot
    private Map<Integer, TexasGame> snapshots;


}
