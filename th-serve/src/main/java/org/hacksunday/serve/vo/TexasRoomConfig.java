package org.hacksunday.serve.vo;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TexasRoomConfig {

    /**
     * 几人场
     */
    private int totalChairCount = 6;
}
