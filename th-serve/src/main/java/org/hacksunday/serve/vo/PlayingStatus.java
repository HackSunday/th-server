package org.hacksunday.serve.vo;

public enum PlayingStatus {
    PREPARED,
    UNPREPARED,
    FOLD,
    ALL_IN,
    NORMAL
}
