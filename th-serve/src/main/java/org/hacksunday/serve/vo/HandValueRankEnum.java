package org.hacksunday.serve.vo;

public enum HandValueRankEnum {
    // 高牌
    HIGH_CARD(0),
    // 单对
    PAIR(1000000),
    // 两对
    TWO_PAIR(2000000),
    // 三条
    THREE_KIND(3000000),
    // 顺子
    STRAIGHT(4000000),
    // 同花
    FLUSH(5000000),
    // 葫芦
    FULL_HOUSE(6000000),
    // 四条
    FOUR_KIND(7000000),
    // 同花顺
    STRAIGHT_FLUSH(8000000),
    // 皇家同花顺
    ROYAL_STRAIGHT_FLUSH(9000000);

    int rank;

    HandValueRankEnum(int rank) {
        this.rank = rank;
    }

    public int getRankNum() {
        return rank;
    }
}
