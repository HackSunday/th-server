package org.hacksunday.serve.vo;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 用户信息
 */
@Setter
@Getter
@EqualsAndHashCode(exclude = {"avatar","name","gender"})
@Builder
public class UserInfo {

    private int id;

    /**
     * 名字
     */
    private String name;

    /**
     * 性别
     */
    private int gender;

    /**
     * 头像
     */
    private String avatar;

}
