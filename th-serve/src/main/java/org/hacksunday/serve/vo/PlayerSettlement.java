package org.hacksunday.serve.vo;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class PlayerSettlement {

    private int pos;

    private int userId;

    private int chips;

    private HandValueRankEnum rankEnum;

    private int rankScore;

    public void addChips(int add) {
        this.chips += add;
    }
}
