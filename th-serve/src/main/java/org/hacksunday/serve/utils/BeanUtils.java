package org.hacksunday.serve.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by onefish on 2016/12/28 0028.
 */
public class BeanUtils {
    static Logger log = LoggerFactory.getLogger(BeanUtils.class);

    /**
     * 对象的拷贝, 支持多个对象的拷贝到同一个目的对象
     *
     * @param targetClazz 目的对象的类
     * @param sources     源对象
     * @param <T>         目的对象类型
     * @return 目的对象
     */
    public static <T> T copyBeans(Class<T> targetClazz, Object... sources) {
        if (null == sources || sources.length == 0) {
            return null;
        }
        T targetObj = null;
        try {
            targetObj = targetClazz.newInstance();
            for (Object sourceObj : sources) {
                BeanCopier copier = BeanCopier.create(sourceObj.getClass(), targetClazz, false);
                copier.copy(sourceObj, targetObj, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return targetObj;
    }

    /**
     * 列表的复制
     *
     * @param sourceList 复制的源列表
     * @param clazz      目的类的CLASS
     * @param <T>        目的对象的类型
     * @return 拷贝的目的对象
     */
    public static <T> List<T> convertFromList(List<? extends Object> sourceList, Class<T> clazz) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return null;
        }
        BeanCopier copier = BeanCopier.create(sourceList.get(0).getClass(), clazz, false);
        List<T> list = new ArrayList<>();
        //这里反射性能可能不佳，要测试 TODO
        try {
            Object target = null;
            for (Object t : sourceList) {
                target = clazz.newInstance();
                copier.copy(t, target, null);
                list.add((T) target);
            }
        } catch (Exception e) {
            log.error("other exception", e);
            throw new RuntimeException("other exception", e);
        }
        return list;
    }

    /**
     * 将list转为map
     *
     * @param list
     * @param keyName
     * @param keyClass
     * @param valueClass
     * @return
     */
    public static <K, V> Map<K, V> list2map(List<V> list, String keyName, Class<K> keyClass, Class<V> valueClass) {
        if (list == null || StringUtils.isEmpty(keyName)) {
            return null;
        }
        Map<K, V> map = new HashMap<K, V>();
        for (V o : list) {
            for (Field field : valueClass.getDeclaredFields()) {
                field.setAccessible(true);
                if (keyName.equals(field.getName())) {
                    try {
                        map.put((K) field.get(o), o);
                    } catch (IllegalArgumentException | IllegalAccessException e) {
                        log.error("", e);
                    }
                    break;
                }
            }
        }
        return map;
    }

    /**
     * arr转为map
     *
     * @param arr
     * @param keyName
     * @param keyClass
     * @param valueClass
     * @return
     */
    public static <K, V> Map<K, V> array2map(V[] arr, String keyName, Class<K> keyClass, Class<V> valueClass) {
        if (arr == null || StringUtils.isEmpty(keyName)) {
            return null;
        }
        Map<K, V> map = new HashMap<K, V>();
        for (V o : arr) {
            for (Field field : valueClass.getDeclaredFields()) {
                field.setAccessible(true);
                if (keyName.equals(field.getName())) {
                    try {
                        map.put((K) field.get(o), o);
                    } catch (IllegalArgumentException | IllegalAccessException e) {
                        log.error("", e);
                    }
                    break;
                }
            }
        }
        return map;
    }
}
