package org.hacksunday.serve.utils;

import io.jsonwebtoken.*;
import org.hacksunday.serve.constant.Constants;
import org.hacksunday.serve.domain.UserDO;

import java.util.Date;

public class JwtUtils {

    private static final JwtParser parser = Jwts.parser().setSigningKey(Constants.JWT_SIGNING_KEY);

    private static final SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

    public static Jws<Claims> parseClaimsJws(String token) {
        return parser.parseClaimsJws(token);
    }

    public static String buildJwt(UserDO userDO) {
        long iat = System.currentTimeMillis();
        long expire = iat + 1000L * 60 * 60 * 24 * 30;
        JwtBuilder jwtBuilder = Jwts.builder().signWith(signatureAlgorithm, Constants.JWT_SIGNING_KEY)
                .setSubject(String.valueOf(userDO.getId())).setIssuedAt(new Date(iat)).setExpiration(new Date(expire));
        return jwtBuilder.compact();
    }
}
