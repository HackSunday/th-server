package org.hacksunday.serve.service;

import lombok.extern.slf4j.Slf4j;
import org.hacksunday.serve.vo.PlayerSettlement;
import org.hacksunday.serve.vo.TexasPlayer;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class SettleService {

    @Resource
    UserAccountService userAccountService;

    /**
     * 结算
     *
     * @param texasPlayers
     * @param settlementList
     */
    @Transactional
    public void settle(Map<Integer, TexasPlayer> texasPlayers, List<PlayerSettlement> settlementList) {
        Map<Integer, PlayerSettlement> settlementMap = new HashMap<>();
        for (PlayerSettlement playerSettlement : settlementList) {
            settlementMap.put(playerSettlement.getPos(), playerSettlement);
        }
        for (Map.Entry<Integer, TexasPlayer> entry : texasPlayers.entrySet()) {
            int pos = entry.getKey();
            TexasPlayer texasPlayer = entry.getValue();
            int winChips = -texasPlayer.getPutinChips();
            if (settlementMap.containsKey(pos)) {
                int chips = settlementMap.get(pos).getChips();
                winChips += chips;
                texasPlayer.addRemainChips(chips);
            }
            // 更新用户的account
            userAccountService.updateByWinChips(texasPlayer, winChips);
            texasPlayer.setPutinChips(0);
        }
    }
}
