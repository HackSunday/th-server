package org.hacksunday.serve.service;

import io.grpc.stub.StreamObserver;
import org.hacksunday.lib.*;
import org.hacksunday.serve.business.TexasGame;
import org.hacksunday.serve.business.TexasHoldemRoom;
import org.hacksunday.serve.vo.Card;
import org.hacksunday.serve.vo.PlayingStatus;
import org.hacksunday.serve.vo.TexasPlayer;
import org.springframework.util.CollectionUtils;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class RuntimeManager {

    private static final Map<Integer, TexasPlayer> players = new ConcurrentHashMap<>();

    private static final Map<Integer, TexasGame> playerGameMap = new ConcurrentHashMap<>();

    private static final Map<Integer, TexasHoldemRoom> playerRoomMap = new ConcurrentHashMap<>();

    private static final Map<Integer, StreamObserver<GrpcRoomReply>> roomObservers = new ConcurrentHashMap<>();

    private static final Map<Integer, StreamObserver<GrpcGameReply>> gameObservers = new ConcurrentHashMap<>();

    public static void putPlayer(int userId, TexasPlayer texasPlayer) {
        players.put(userId, texasPlayer);
    }

    public static void putRoom(int userId, TexasHoldemRoom room) {
        playerRoomMap.put(userId, room);
    }

    public static void putGame(int userId, TexasGame game) {
        playerGameMap.put(userId, game);
    }

    public static void putRoomObserver(int userId, StreamObserver<GrpcRoomReply> observer) {
        roomObservers.put(userId, observer);
    }

    public static void putGameObserver(int userId, StreamObserver<GrpcGameReply> observer) {
        gameObservers.put(userId, observer);
    }

    public static boolean isPlaying(int userId) {
        return playerGameMap.containsKey(userId);
    }

    public static boolean isConnecting(int userId) {
        return players.containsKey(userId);
    }

    public static boolean isInRoom(int userId) {
        return playerRoomMap.containsKey(userId);
    }

    public static TexasPlayer getPlayer(int userId) {
        return players.get(userId);
    }

    public static TexasGame getGame(int userId) {
        return playerGameMap.get(userId);
    }

    public static TexasHoldemRoom getRoom(int userId) {
        return playerRoomMap.get(userId);
    }

    public static StreamObserver<GrpcRoomReply> getRoomObserver(int userId) {
        return roomObservers.get(userId);
    }

    public static StreamObserver<GrpcGameReply> getGameObserver(int userId) {
        return gameObservers.get(userId);
    }

    public static void removePlayer(int userId) {
        players.remove(userId);
    }

    public static void removeRoom(int userId) {
        playerRoomMap.remove(userId);
    }

    public static void removeGame(TexasGame texasGame) {
        for (TexasPlayer value : texasGame.getTexasPlayers().values()) {
            playerGameMap.remove(value.getUserId());
        }
    }

    public static void notifyRoom(TexasHoldemRoom texasHoldemRoom) {
        GrpcRoom grpcRoom = buildGrpcRoom(texasHoldemRoom);
        for (TexasPlayer texasPlayer : texasHoldemRoom.getTexasPlayers().values()) {
            notifyRoom(texasPlayer.getUserId(), grpcRoom);
        }
    }

    public static void notifyRoom(int userId, GrpcRoom grpcRoom) {
        GrpcRoomReply grpcRoomReply = GrpcRoomReply.newBuilder().setData(grpcRoom).build();
        notifyRoom(userId, grpcRoomReply);
    }

    public static void notifyRoom(int userId, GrpcRoomReply reply) {
        Optional.ofNullable(getRoomObserver(userId)).ifPresent(observer -> observer.onNext(reply));
    }

    public static GrpcRoom buildGrpcRoom(TexasHoldemRoom texasHoldemRoom) {
        GrpcRoom.Builder builder = GrpcRoom.newBuilder();
        // room config
        builder.setRoomId(texasHoldemRoom.getRoomId());
        GrpcTexasGameConfig texasGameConfig = GrpcTexasGameConfig.newBuilder()
                .setTotalChairCount(texasHoldemRoom.getTexasGameConfig().getTotalChairCount())
                .setTimeoutSeconds(texasHoldemRoom.getTexasGameConfig().getTimeoutSeconds())
                .setSmallBlind(texasHoldemRoom.getTexasGameConfig().getSmallBlind()).build();
        builder.setConfig(texasGameConfig);
        // room player信息
        for (Map.Entry<Integer, TexasPlayer> playerEntry : texasHoldemRoom.getTexasPlayers().entrySet()) {
            TexasPlayer texasPlayer = playerEntry.getValue();
            builder.putPlayers(playerEntry.getKey(), buildRoomPlayer(texasPlayer));
        }
        return builder.build();
    }

    public static void notifyGame(TexasGame texasGame) {
        GrpcTexasGame grpcTexasGame = buildGrpcTexasGame(texasGame);
        for (TexasPlayer texasPlayer : texasGame.getTexasPlayers().values()) {
            int userId = texasPlayer.getUserId();
            notifyGame(userId, grpcTexasGame);
        }
    }

    public static GrpcTexasGame hideHandCards(int userId, GrpcTexasGame origin) {
        GrpcTexasGame.Builder clone = GrpcTexasGame.newBuilder().mergeFrom(origin);
        for (Map.Entry<Integer, GrpcTexasPlayer> playerEntry : clone.getPlayersMap().entrySet()) {
            GrpcTexasPlayer grpcTexasPlayer = handleShowCard(userId, playerEntry.getValue(), clone.build());
            clone.putPlayers(playerEntry.getKey(), grpcTexasPlayer);
        }
        return clone.build();
    }

    public static void notifyGame(int userId, GrpcTexasGame game) {
        GrpcTexasGame grpcTexasGame = hideHandCards(userId, game);
        notifyGame(userId, GrpcGameReply.newBuilder().setData(grpcTexasGame).build());
    }

    public static void notifyGame(int userId, GrpcGameReply reply) {
        Optional.ofNullable(getGameObserver(userId)).ifPresent(observer -> observer.onNext(reply));
    }

    public static GrpcTexasGame buildGrpcTexasGame(TexasGame texasGame) {
        GrpcTexasGameConfig texasGameConfig = GrpcTexasGameConfig.newBuilder()
                .setTotalChairCount(texasGame.getTexasGameConfig().getTotalChairCount())
                .setTimeoutSeconds(texasGame.getTexasGameConfig().getTimeoutSeconds())
                .setSmallBlind(texasGame.getTexasGameConfig().getSmallBlind()).build();
        GrpcTexasGame.Builder gameBuilder = GrpcTexasGame.newBuilder();
        gameBuilder.setConfig(texasGameConfig)
                .setCurrentPos(texasGame.getCurrentPos())
                .setDealerPos(texasGame.getDealerPos())
                .setRound(texasGame.getRoundEnum())
                .setRemainSeconds(texasGame.getTexasGameConfig().getTimeoutSeconds());
        if (!CollectionUtils.isEmpty(texasGame.getCardPool())) {
            for (Card card : texasGame.getCardPool()) {
                gameBuilder.addCardPool(buildCard(card));
            }
        }
        for (Map.Entry<Integer, TexasPlayer> playerEntry : texasGame.getTexasPlayers().entrySet()) {
            gameBuilder.putPlayers(playerEntry.getKey(), buildPlayer(playerEntry.getValue()));
        }
        return gameBuilder.build();
    }

    private static GrpcTexasPlayer handleShowCard(Integer userId, GrpcTexasPlayer player, GrpcTexasGame game) {
        GrpcTexasPlayer.Builder builder = player.toBuilder();
        if (!showPlayerCard(userId, player, game)) {
            builder.clearCards();
            for (int i = 0; i < player.getCardsList().size(); i++) {
                builder.addCards(GrpcCard.newBuilder().setRank(GrpcCardRank.HIDE_RANK)
                        .setSuit(GrpcCardSuit.HIDE_SUIT));
            }
        }
        return builder.build();
    }

    private static boolean showPlayerCard(Integer userId, GrpcTexasPlayer value, GrpcTexasGame game) {
        // 自己的手牌
        if (value.getUserId() == userId) {
            return true;
        }
        // 斗牌阶段，未fold fixme 后续增加show down阶段操作，可以自行决定翻牌或者muck
        if (game.getRound().getNumber() >= GrpcRoundEnum.SHOW_DOWN.getNumber()) {
            return !value.getStatus().equals(GrpcPlayingStatus.FOLDED);
        }
        return false;
    }

    private static GrpcRoomPlayer buildRoomPlayer(TexasPlayer texasPlayer) {
        GrpcRoomPlayer.Builder playerBuilder = GrpcRoomPlayer.newBuilder();
        playerBuilder.setUserId(texasPlayer.getUserId());
        playerBuilder.setUserName(texasPlayer.getUserName());
        playerBuilder.setUserAccount(texasPlayer.getUserAccount());
        playerBuilder.setTakeChips(texasPlayer.getRemainChips());
        playerBuilder.setStatus(getStatus(texasPlayer.getPlayingStatus()));
        return playerBuilder.build();
    }

    private static GrpcTexasPlayer buildPlayer(TexasPlayer texasPlayer) {
        GrpcTexasPlayer.Builder playerBuilder = GrpcTexasPlayer.newBuilder();
        playerBuilder.setUserId(texasPlayer.getUserId());
        playerBuilder.setPutInChips(texasPlayer.getPutinChips());
        playerBuilder.setRemainChips(texasPlayer.getRemainChips());
        playerBuilder.setStatus(getStatus(texasPlayer.getPlayingStatus()));
        if (!CollectionUtils.isEmpty(texasPlayer.getCards())) {
            for (Card card : texasPlayer.getCards()) {
                playerBuilder.addCards(buildCard(card));
            }
        }
        return playerBuilder.build();
    }

    private static GrpcCard buildCard(Card card) {
        GrpcCard.Builder builder = GrpcCard.newBuilder();
        switch (card.getSuit()) {
            case CLUBS:
                builder.setSuit(GrpcCardSuit.CLUBS);
                break;
            case HEARTS:
                builder.setSuit(GrpcCardSuit.HEARTS);
                break;
            case SPADES:
                builder.setSuit(GrpcCardSuit.SPADES);
                break;
            case DIAMONDS:
                builder.setSuit(GrpcCardSuit.DIAMONDS);
                break;
        }

        switch (card.getRank()) {
            case CARD_TWO:
                builder.setRank(GrpcCardRank.TWO);
                break;
            case CARD_THREE:
                builder.setRank(GrpcCardRank.THREE);
                break;
            case CARD_FOUR:
                builder.setRank(GrpcCardRank.FOUR);
                break;
            case CARD_FIVE:
                builder.setRank(GrpcCardRank.FIVE);
                break;
            case CARD_SIX:
                builder.setRank(GrpcCardRank.SIX);
                break;
            case CARD_SEVEN:
                builder.setRank(GrpcCardRank.SEVEN);
                break;
            case CARD_EIGHT:
                builder.setRank(GrpcCardRank.EIGHT);
                break;
            case CARD_NINE:
                builder.setRank(GrpcCardRank.NINE);
                break;
            case CARD_TEN:
                builder.setRank(GrpcCardRank.TEN);
                break;
            case CARD_JACK:
                builder.setRank(GrpcCardRank.JACK);
                break;
            case CARD_QUEEN:
                builder.setRank(GrpcCardRank.QUEEN);
                break;
            case CARD_KING:
                builder.setRank(GrpcCardRank.KING);
                break;
            case CARD_ACE:
                builder.setRank(GrpcCardRank.ACE);
                break;
        }
        return builder.build();
    }


    private static GrpcPlayingStatus getStatus(PlayingStatus playingStatus) {
        switch (playingStatus) {
            case FOLD:
                return GrpcPlayingStatus.FOLDED;
            case NORMAL:
                return GrpcPlayingStatus.NORMAL;
            case PREPARED:
                return GrpcPlayingStatus.PREPARED;
            case UNPREPARED:
                return GrpcPlayingStatus.UNPREPARED;
            case ALL_IN:
                return GrpcPlayingStatus.SHOWHAND;
        }
        return GrpcPlayingStatus.UNRECOGNIZED;
    }


}
