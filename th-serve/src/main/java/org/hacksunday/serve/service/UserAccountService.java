package org.hacksunday.serve.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.hacksunday.lib.GrpcUserAccount;
import org.hacksunday.serve.domain.UserAccountDO;
import org.hacksunday.serve.mapper.UserAccountMapper;
import org.hacksunday.serve.vo.TexasPlayer;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author onefish
 * @since 2021-07-05
 */
@Service
public class UserAccountService extends ServiceImpl<UserAccountMapper, UserAccountDO> implements IService<UserAccountDO> {

    public GrpcUserAccount getByUserId(Integer userId) {
        return buildUserAccount(getByUserId_(userId));
    }

    private UserAccountDO getByUserId_(Integer userId) {
        LambdaQueryWrapper<UserAccountDO> lambdaQueryWrapper = Wrappers.lambdaQuery();
        lambdaQueryWrapper.eq(UserAccountDO::getUserId, userId);
        return getOne(lambdaQueryWrapper);
    }

    private GrpcUserAccount buildUserAccount(UserAccountDO userAccountDO) {
        return GrpcUserAccount.newBuilder().setCoins(userAccountDO.getCoins()).setLevel(userAccountDO.getLevel()).setId(userAccountDO.getId()).build();
    }

    /**
     * @param texasPlayer
     * @param winChips    可以为负数
     */
    public void updateByWinChips(TexasPlayer texasPlayer, int winChips) {
        GrpcUserAccount userAccount = texasPlayer.getUserAccount();
        getBaseMapper().addChips(userAccount.getId(), winChips);
        texasPlayer.setUserAccount(userAccount.toBuilder().setCoins(userAccount.getCoins() + winChips).build());
    }
}
