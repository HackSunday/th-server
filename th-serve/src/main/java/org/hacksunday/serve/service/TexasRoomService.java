package org.hacksunday.serve.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import org.hacksunday.lib.GrpcRoom;
import org.hacksunday.lib.GrpcRoomCommand;
import org.hacksunday.lib.GrpcRoomCommandType;
import org.hacksunday.lib.GrpcRoomReply;
import org.hacksunday.serve.business.TexasHoldemRoom;
import org.hacksunday.serve.constant.Constants;
import org.hacksunday.serve.ex.BusinessException;
import org.hacksunday.serve.vo.PlayingStatus;
import org.hacksunday.serve.vo.TexasGameConfig;
import org.hacksunday.serve.vo.TexasPlayer;
import org.hacksunday.serve.vo.param.JoinRoomParam;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class TexasRoomService {

    @Resource
    TexasGameService texasGameService;

    private final static AtomicInteger roomId = new AtomicInteger(1);

    private final static Random random = new Random();

    private final static Map<Integer, TexasHoldemRoom> roomMap = new ConcurrentHashMap<>();

    public void handleCommand(GrpcRoomCommand command, StreamObserver<GrpcRoomReply> responseObserver) {
        int userId = Integer.parseInt(Constants.USER_ID_CONTEXT_KEY.get());
        switch (command.getCommand()) {
            case CONNECT_ROOM:
                connect(userId);
                break;
            case CREATE:
                createRoom(userId, command);
                break;
            case JOIN:
                joinRoom(userId, command);
                break;
            case PREPARE:
                prepare(userId);
                break;
            case UNPREPARE:
                unPrepare(userId);
                break;
            case QUIT:
                quit(userId, command);
                break;
            case TAKE_CHIPS:
                takeChips(userId, command);
        }
    }

    private void connect(int userId) {
        RuntimeManager.notifyRoom(userId, GrpcRoomReply.newBuilder().setCode(200).setMsg("连接成功").build());
        // 如果当前用户正在房间中，直接返回房间信息
        if (RuntimeManager.isInRoom(userId)) {
            GrpcRoom grpcRoom = RuntimeManager.buildGrpcRoom(RuntimeManager.getRoom(userId));
            RuntimeManager.notifyRoom(userId, grpcRoom);
        }
    }

    /**
     * 退出房间
     * 如果当前有对局，房间退出后仍然可以重连，对局结束后自动退出房间
     *
     * @param userId
     * @param command
     */
    private void quit(int userId, GrpcRoomCommand command) {
        boolean force = JSONObject.parseObject(command.getBody()).getBooleanValue("force");
        if (!force && RuntimeManager.isPlaying(userId)) {
            RuntimeManager.notifyRoom(userId, GrpcRoomReply.newBuilder().setCode(10).setMsg("当前有正在进行的游戏，确认退出吗？").build());
            return;
        }
        if (RuntimeManager.isInRoom(userId)) {
            TexasHoldemRoom room = RuntimeManager.getRoom(userId);
            room.quitRoom(userId);
            RuntimeManager.removeRoom(userId);
            RuntimeManager.notifyRoom(room);
            if (room.isEmpty()) {
                roomMap.remove(room.getRoomId());
            }
        }
    }

    private void joinRoom(int userId, GrpcRoomCommand gameCommand) {
        validCreateOrJoinRoom(userId);
        JoinRoomParam param = JSON.parseObject(gameCommand.getBody(), JoinRoomParam.class);
        TexasHoldemRoom texasHoldemRoom = joinRoom(userId, param.getRoomId());
        RuntimeManager.notifyRoom(texasHoldemRoom);
    }

    /**
     * 创建房间
     *
     * @param userId
     * @param gameCommand
     */
    private void createRoom(int userId, GrpcRoomCommand gameCommand) {
        validCreateOrJoinRoom(userId);
        TexasHoldemRoom texasHoldemRoom = create(userId, gameCommand);
        RuntimeManager.notifyRoom(texasHoldemRoom);
    }

    private void unPrepare(int userId) {
        RuntimeManager.getPlayer(userId).setPlayingStatus(PlayingStatus.UNPREPARED);
    }

    private void prepare(int userId) {
        if (!RuntimeManager.isInRoom(userId)) {
            throw new BusinessException(Status.NOT_FOUND.withDescription("还未进入房间"));
        }
        TexasPlayer texasPlayer = RuntimeManager.getPlayer(userId);
        TexasHoldemRoom room = RuntimeManager.getRoom(userId);
        // 检验筹码是否足够大盲
        if (texasPlayer.getRemainChips() <= room.getTexasGameConfig().getSmallBlind() * 2) {
            throw new BusinessException(Status.RESOURCE_EXHAUSTED.withDescription("当前筹码不够"));
        }
        // 用户状态变更为已准备
        texasPlayer.setPlayingStatus(PlayingStatus.PREPARED);
        RuntimeManager.notifyRoom(room);

        // 如果玩家全部准备，且玩家数大于1人 则自动开始
        if (room.allPrepared() && room.getTexasPlayers().size() > 1) {
            // 游戏开始
            texasGameService.start(room);
        }
    }

    private TexasHoldemRoom create(int userId, GrpcRoomCommand gameCommand) {
        // 创建房间
        TexasHoldemRoom texasHoldemRoom = new TexasHoldemRoom();
        String body = gameCommand.getBody();
        TexasGameConfig gameConfig = new TexasGameConfig();
        if (StringUtils.hasText(body)) {
            gameConfig = JSONObject.parseObject(body, TexasGameConfig.class);
        }
        texasHoldemRoom.setTexasGameConfig(gameConfig);
        texasHoldemRoom.setRoomId(roomId.getAndIncrement());
        int pos = random.nextInt(gameConfig.getTotalChairCount());
        // 房主进入房间
        TexasPlayer player = RuntimeManager.getPlayer(userId);
        texasHoldemRoom.setDealerPos(pos);
        texasHoldemRoom.getTexasPlayers().put(pos, player);
        // 携带筹码
        takeMinChips(player, gameConfig);
        // 保存runtime数据
        roomMap.put(texasHoldemRoom.getRoomId(), texasHoldemRoom);
        RuntimeManager.putRoom(userId, texasHoldemRoom);

        // todo 持久化
        return texasHoldemRoom;
    }

    private void validCreateOrJoinRoom(int userId) {
        // 如果已经在游戏中
        if (RuntimeManager.isPlaying(userId)) {
            throw new BusinessException(Status.ALREADY_EXISTS.withDescription("当前有正在进行的游戏，无法加入房间"));
        }
        // 如果已在其他房间
        if (RuntimeManager.isInRoom(userId)) {
            // 先退出房间
            JSONObject body = new JSONObject();
            body.put("force", true);
            quit(userId, GrpcRoomCommand.newBuilder().setCommand(GrpcRoomCommandType.QUIT).setBody(body.toJSONString()).build());
        }
    }

    private boolean takeChips(int userId, GrpcRoomCommand command) {
        TexasPlayer player = RuntimeManager.getPlayer(userId);
        int chips = JSON.parseObject(command.getBody()).getIntValue("chips");
        return takeChips(player, chips);
    }

    private boolean takeChips(TexasPlayer player, int chips) {
        if (player.getUserAccount().getCoins() < chips) {
            throw new BusinessException(Status.RESOURCE_EXHAUSTED.withDescription("筹码不够"));
        }
        player.setRemainChips(chips);
        player.setPutinChips(0);
        return true;
    }

    private boolean takeMinChips(TexasPlayer player, TexasGameConfig gameConfig) {
        // 校验筹码够不够
        int minChips = gameConfig.getSmallBlind() * 50;
        return takeChips(player, minChips);
    }

    private TexasHoldemRoom joinRoom(int userId, int roomId) {
        if (!roomMap.containsKey(roomId)) {
            throw new BusinessException(Status.NOT_FOUND.withDescription("房间不存在"));
        }
        TexasHoldemRoom texasHoldemRoom = roomMap.get(roomId);
        if (texasHoldemRoom.isFull()) {
            throw new BusinessException(Status.RESOURCE_EXHAUSTED.withDescription("房间已满"));
        }
        texasHoldemRoom.joinRoom(RuntimeManager.getPlayer(userId));
        // 携带筹码
        takeMinChips(RuntimeManager.getPlayer(userId), texasHoldemRoom.getTexasGameConfig());
        RuntimeManager.putRoom(userId, texasHoldemRoom);
        return texasHoldemRoom;
    }

}
