package org.hacksunday.serve.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.hacksunday.lib.*;
import org.hacksunday.serve.business.ChipsPool;
import org.hacksunday.serve.business.TexasGame;
import org.hacksunday.serve.business.TexasHoldemRoom;
import org.hacksunday.serve.constant.Constants;
import org.hacksunday.serve.ex.BusinessException;
import org.hacksunday.serve.vo.PlayerSettlement;
import org.hacksunday.serve.vo.PlayingStatus;
import org.hacksunday.serve.vo.TexasPlayer;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class TexasGameService {

    @Resource
    TexasRoomService texasRoomService;

    @Resource
    SettleService settleService;

    public void handleCommand(GrpcGameCommand gameCommand, StreamObserver<GrpcGameReply> responseObserver) {
        int userId = Integer.parseInt(Constants.USER_ID_CONTEXT_KEY.get());
        // 校验command是否合法
        validCommand(userId, gameCommand);
        try {
            switch (gameCommand.getCommand()) {
                case CONNECT_GAME:
                    connect(userId);
                    return;
                case BET:
                    bet(userId, gameCommand);
                    break;
                case CALL:
                    call(userId);
                    break;
                case FOLD:
                    fold(userId);
                    break;
                case CHECK:
                    check(userId);
                    break;
                case RAISE:
                    raise(userId, gameCommand);
                    break;
                case ALL_IN:
                    allIn(userId);
                    break;
            }
            // 用户操作之后处理
            afterUserOp(userId);
        } catch (Exception e) {
            log.error("Game Command {} on Error", gameCommand.getCommand(), e);
            if (e instanceof BusinessException) {
                // 将所有的业务异常转换为错误码返回
                RuntimeManager.notifyGame(userId, GrpcGameReply.newBuilder().setCode(((BusinessException) e).getStatus().getCode().value()).setMsg(e.getMessage()).build());
            } else {
                if ((e instanceof StatusRuntimeException) || (e instanceof StatusException)) {
                    responseObserver.onError(e);
                } else {
                    responseObserver.onError(new BusinessException(Status.INTERNAL.withCause(e).withDescription(e.getMessage())));
                }
            }
        }
    }

    private void afterUserOp(int userId) {
        Optional.ofNullable(RuntimeManager.getGame(userId)).ifPresent(texasGame -> {
            if (!texasGame.needOp()) {
                while (true) {
                    if (texasGame.shouldOver()) {
                        over(texasGame);
                        break;
                    } else {
                        texasGame.shouldGoNextRound();
                    }
                }
            } else {
                if (texasGame.shouldOver()) {
                    over(texasGame);
                } else {
                    texasGame.shouldGoNextRound();
                }
            }
        });
    }


    private void allIn(int userId) {
        TexasGame texasGame = RuntimeManager.getGame(userId);
        texasGame.allIn(userId);
        RuntimeManager.notifyGame(texasGame);
    }

    private void raise(int userId, GrpcGameCommand gameCommand) {
        JSONObject param = JSON.parseObject(gameCommand.getBody());
        Integer chips = param.getInteger("chips");
        TexasGame texasGame = RuntimeManager.getGame(userId);
        texasGame.raise(userId, chips);
        RuntimeManager.notifyGame(texasGame);
    }

    private void check(int userId) {
        TexasGame texasGame = RuntimeManager.getGame(userId);
        texasGame.check(userId);
        RuntimeManager.notifyGame(texasGame);
    }

    private void fold(int userId) {
        TexasGame texasGame = RuntimeManager.getGame(userId);
        texasGame.fold(userId);
        texasGame.moveNextOpPos();
        RuntimeManager.notifyGame(texasGame);
    }

    private void call(int userId) {
        TexasGame texasGame = RuntimeManager.getGame(userId);
        texasGame.call(userId);
        RuntimeManager.notifyGame(texasGame);
    }


    private void bet(int userId, GrpcGameCommand gameCommand) {
        // 未能区分bet和raise的区别，暂不实现bet，全部用raise
        throw new BusinessException(Status.UNAVAILABLE.withDescription("bet not support"));
    }

    // todo 校验command是否合法
    private void validCommand(int userId, GrpcGameCommand gameCommand) {
        if (gameCommand.getCommand().equals(GrpcCommandType.CONNECT_GAME)) {
            return;
        }
        if (!RuntimeManager.isPlaying(userId)) {
            throw new BusinessException(Status.PERMISSION_DENIED.withDescription("当前玩家不在游戏中"));
        }
        // 是否轮到当前用户操作
        TexasGame texasGame = RuntimeManager.getGame(userId);
        int userPos = texasGame.getUserPos(userId);
        if (texasGame.getCurrentPos() != userPos) {
            throw new BusinessException(Status.PERMISSION_DENIED.withDescription("还未到您的操作回合"));
        }
        // 校验操作是否允许
        switch (gameCommand.getCommand()) {
            case CHECK:
                if (!texasGame.getChipsPool().canCheck(userPos)) {
                    throw new BusinessException(Status.PERMISSION_DENIED.withDescription("当前无法CHECK"));
                }
                break;
            case RAISE:
                if (!texasGame.getChipsPool().canRaise(userPos)) {
                    throw new BusinessException(Status.PERMISSION_DENIED.withDescription("当前无法RAISE"));
                }
                break;
            case CALL:
                if (!texasGame.getChipsPool().canCall(userPos)) {
                    throw new BusinessException(Status.PERMISSION_DENIED.withDescription("当前无法CALL"));
                }
                break;
        }
    }


    /**
     * 游戏开始
     *
     * @param texasHoldemRoom
     */
    public void start(TexasHoldemRoom texasHoldemRoom) {
        // 初始化游戏
        TexasGame texasGame = new TexasGame();
        texasGame.setTexasGameConfig(texasHoldemRoom.getTexasGameConfig());
        texasGame.setDealerPos(texasHoldemRoom.getDealerPos());
        texasGame.setCurrentPos(texasHoldemRoom.getDealerPos());
        texasGame.setRoundEnum(GrpcRoundEnum.PRE_FLOP);
        // 所有已准备的玩家进入游戏 玩家状态变为正常状态
        texasHoldemRoom.getTexasPlayers().forEach((pos, player) -> {
            // 情况玩家手牌
            player.setCards(new ArrayList<>());
            if (player.getPlayingStatus().equals(PlayingStatus.PREPARED)) {
                player.setPlayingStatus(PlayingStatus.NORMAL);
                texasGame.getTexasPlayers().put(pos, player);
                // 保存用户的runtime
                RuntimeManager.putGame(player.getUserId(), texasGame);
            }
        });
        // 初始化注池
        ChipsPool chipsPool = new ChipsPool(texasGame.getTexasPlayers());
        chipsPool.setDealerPos(texasGame.getDealerPos());
        chipsPool.setSmallBlind(texasGame.getTexasGameConfig().getSmallBlind());

        texasGame.setChipsPool(chipsPool);
        // 进入preFlop
        texasGame.preFlop();
        texasHoldemRoom.setTexasGame(texasGame);
        RuntimeManager.notifyRoom(texasHoldemRoom);
    }

    private void over(TexasGame texasGame) {
        // 游戏进行结算
        List<PlayerSettlement> playerSettlements = texasGame.settle();
        // 持久化结算
        settleService.settle(texasGame.getTexasPlayers(), playerSettlements);
        // 游戏状态变更为结束
        texasGame.setRoundEnum(GrpcRoundEnum.OVER);
        // 所有玩家状态变更为未准备
        for (TexasPlayer value : texasGame.getTexasPlayers().values()) {
            value.setPlayingStatus(PlayingStatus.UNPREPARED);
        }
        // 房间的庄家跳转到下一位
        TexasHoldemRoom room = RuntimeManager.getRoom(texasGame.getTexasPlayers().values().iterator().next().getUserId());
        room.moveNextDealer();
        RuntimeManager.notifyGame(texasGame);
        // 释放game资源
        RuntimeManager.removeGame(texasGame);
        RuntimeManager.notifyRoom(room);
    }

    /**
     * 如果有正在进行的牌局或者在房间内，则重连
     *
     * @param userId
     */
    private void connect(int userId) {
        RuntimeManager.notifyGame(userId, GrpcGameReply.newBuilder().setCode(10).setMsg("连接游戏服务器成功").build());
        if (RuntimeManager.isPlaying(userId)) {
            TexasGame game = RuntimeManager.getGame(userId);
            GrpcTexasGame grpcTexasGame = RuntimeManager.buildGrpcTexasGame(game);
            RuntimeManager.notifyGame(userId, grpcTexasGame);
        }
        if (RuntimeManager.isInRoom(userId)) {
            TexasHoldemRoom room = RuntimeManager.getRoom(userId);
            RuntimeManager.notifyRoom(room);
        }
    }
}
