package org.hacksunday.serve.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.hacksunday.lib.*;
import org.hacksunday.serve.domain.UserAccountDO;
import org.hacksunday.serve.domain.UserDO;
import org.hacksunday.serve.mapper.UsersMapper;
import org.hacksunday.serve.utils.JwtUtils;
import org.hacksunday.serve.utils.Md5Utils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
public class UserService extends ServiceImpl<UsersMapper, UserDO> implements IService<UserDO> {

    @Resource
    UserAccountService userAccountService;

    /**
     * 登录
     *
     * @param request
     * @return
     */
    public LoginResponseVo login(LoginRequest request) {
        UserDO userDO = getByMobile(request.getMobile());
        if (null == userDO) {
            return LoginResponseVo.newBuilder().setCode(400).setMsg("账号/密码错误").build();
        }
        String md5 = Md5Utils.getMD5(request.getPassword());
        if (!md5.equals(userDO.getPasswordHash())) {
            return LoginResponseVo.newBuilder().setCode(400).setMsg("账号/密码错误").build();
        }
        GrpcUserAccount userAccount = userAccountService.getByUserId(userDO.getId());
        LoginResponse loginResponse = LoginResponse.newBuilder()
                .setId(userDO.getId()).setGender(userDO.getGender()).setAvatar(userDO.getAvatar()).setUserName(userDO.getUserName())
                .setMobile(userDO.getPhoneNumber()).setJwt(JwtUtils.buildJwt(userDO)).setAccount(userAccount)
                .build();
        return LoginResponseVo.newBuilder().setCode(200).setData(loginResponse).build();
    }

    /**
     * 注册
     *
     * @param request
     * @return
     */
    @Transactional
    public SignUpResponseVo signUp(SignUpRequest request) {
        // 手机号不能重复
        UserDO byMobile = getByMobile(request.getMobile());
        if (null != byMobile) {
            return SignUpResponseVo.newBuilder().setCode(400).setMsg("手机号重复，请更换手机号重新注册").build();
        }
        // 创建用户信息
        UserDO userDO = UserDO.builder().userName(request.getUserName()).gender(request.getGender())
                .phoneNumber(request.getMobile()).passwordHash(Md5Utils.getMD5(request.getPassword())).build();
        save(userDO);
        int userId = userDO.getId();
        // 初始化等级为1级 coins为5000
        UserAccountDO userAccountDO = UserAccountDO.builder().userId(userId).coins(5000L).level(1).build();
        userAccountService.save(userAccountDO);
        String jwt = JwtUtils.buildJwt(userDO);

        return SignUpResponseVo.newBuilder().setCode(200).setData(SignUpResponse.newBuilder().setUserId(userId).setJwt(jwt).build()).build();
    }

    public UserDO getByMobile(String mobile) {
        LambdaQueryWrapper<UserDO> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(UserDO::getPhoneNumber, mobile);
        return getOne(queryWrapper);
    }
}
