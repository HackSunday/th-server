package org.hacksunday.serve.service;

import org.hacksunday.lib.GrpcUserAccount;
import org.hacksunday.serve.domain.UserDO;
import org.hacksunday.serve.vo.TexasPlayer;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class PlayerService {

    @Resource
    UserService userService;

    @Resource
    UserAccountService userAccountService;

    @Resource
    TexasGameService texasGameService;

    /**
     * 玩家连接
     *
     * @param userId
     */
    public void connect(int userId) {
        TexasPlayer player = RuntimeManager.getPlayer(userId);
        if (null == player) {
            player = new TexasPlayer();
            UserDO userDO = userService.getById(userId);
            if (null != userDO) {
                player.setUserId(userId);
                player.setUserName(userDO.getUserName());
                GrpcUserAccount userAccount = userAccountService.getByUserId(userId);
                player.setUserAccount(userAccount);
            }
        }
        RuntimeManager.putPlayer(userId, player);
    }

    /**
     * 玩家断开连接，释放内存
     *
     * @param userId
     */
    public void disconnect(int userId) {
        RuntimeManager.removePlayer(userId);
    }

    /**
     * 查询玩家信息
     *
     * @param userId
     * @return
     */
    public TexasPlayer get(int userId) {
        return RuntimeManager.getPlayer(userId);
    }

}
