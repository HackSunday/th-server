package org.hacksunday.serve.business;

import lombok.Getter;
import lombok.Setter;
import org.hacksunday.serve.vo.PlayingStatus;
import org.hacksunday.serve.vo.TexasGameConfig;
import org.hacksunday.serve.vo.TexasPlayer;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 德州游戏房
 */
@Setter
@Getter
public class TexasHoldemRoom {

    private int roomId;

    /**
     * 游戏配置
     */
    private TexasGameConfig texasGameConfig;

    /**
     * 在房间的玩家信息
     * (座位，玩家)
     */
    private Map<Integer, TexasPlayer> texasPlayers = new ConcurrentHashMap<>();

    /**
     * 游戏信息
     */
    private TexasGame texasGame;

    private int dealerPos = 0;

    public boolean allPrepared() {
        for (TexasPlayer value : texasPlayers.values()) {
            if (value.getPlayingStatus().equals(PlayingStatus.UNPREPARED)) {
                return false;
            }
        }
        return true;
    }

    public void joinRoom(TexasPlayer player) {
        texasPlayers.put(getAvailablePos(), player);
    }

    public int getAvailablePos() {
        List<Integer> allPos = new ArrayList<>();
        for (int i = 0; i < texasGameConfig.getTotalChairCount(); i++) {
            if (!texasPlayers.containsKey(i)) {
                allPos.add(i);
            }
        }
        return allPos.get(new Random().nextInt(allPos.size()));
    }

    public void quitRoom(int userId) {
        int pos = 0;
        for (Map.Entry<Integer, TexasPlayer> playerEntry : texasPlayers.entrySet()) {
            if (userId == playerEntry.getValue().getUserId()) {
                pos = playerEntry.getKey();
                break;
            }
        }
        texasPlayers.remove(pos);
        // 如果当前用户是庄家，还需要顺延庄家
        if (!isEmpty() && dealerPos == pos) {
            moveNextDealer();
        }
    }

    public boolean isEmpty() {
        return CollectionUtils.isEmpty(texasPlayers);
    }

    public boolean isFull() {
        return texasGameConfig.getTotalChairCount() <= texasPlayers.size();
    }

    public void moveNextDealer() {
        int nextPos = dealerPos;
        for (int i = 0; i < texasGameConfig.getTotalChairCount(); i++) {
            nextPos++;
            nextPos = nextPos >= texasGameConfig.getTotalChairCount() ? nextPos % texasGameConfig.getTotalChairCount() : nextPos;
            if (texasPlayers.containsKey(nextPos)) {
                dealerPos = nextPos;
                return;
            }
        }
        throw new IllegalStateException("无法找到下一位玩家");
    }
}
