package org.hacksunday.serve.business.comparing;

import org.hacksunday.serve.vo.Card;
import org.hacksunday.serve.vo.HandValueRankEnum;

import java.util.List;

public class RoyalStraightFlush implements HandValue {

    private List<Card> cards;

    public RoyalStraightFlush(List<Card> cards) {
        this.cards = cards;
    }

    @Override
    public HandValueRankEnum getRank() {
        return HandValueRankEnum.ROYAL_STRAIGHT_FLUSH;
    }

    @Override
    public List<Card> getCards() {
        return cards;
    }

    @Override
    public int getRankScore() {
        return getRank().getRankNum();
    }

//    @Override
//    public int sameRankCompare(HandValue o) {
//        // 两个皇家同花顺是相等的
//        return 0;
//    }
}
