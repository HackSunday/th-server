package org.hacksunday.serve.business.comparing;

import lombok.Getter;
import org.hacksunday.serve.vo.Card;
import org.hacksunday.serve.vo.HandValueRankEnum;

import java.util.Collections;
import java.util.List;

@Getter
public class TwoPair implements HandValue {

    private List<Card> pairs;

    private Card highCard;

    private List<Card> cards;

    private int rankScore;

    public TwoPair(List<Card> pairs, Card highCard, List<Card> cards) {
        this.pairs = pairs;
        this.highCard = highCard;
        this.cards = cards;
        calcRankScore();
    }

    private void calcRankScore() {
        StringBuilder hex = new StringBuilder();
        for (Card card : pairs) {
            hex.append(Integer.toHexString(card.getRankNumber()));
        }
        hex.append(Integer.toHexString(highCard.getRankNumber()));
        this.rankScore = getRank().getRankNum() + Integer.parseInt(hex.toString(), 16);
    }

    @Override
    public HandValueRankEnum getRank() {
        return HandValueRankEnum.TWO_PAIR;
    }

    @Override
    public List<Card> getCards() {
        return cards;
    }

//    @Override
//    public int sameRankCompare(HandValue o) {
//        TwoPair other = (TwoPair) o;
//        Collections.sort(other.getPairs());
//        Collections.sort(pairs);
//        // 先比较对子大小
//        for (int i = 0; i < pairs.size(); i++) {
//            int compared = other.getPairs().get(i).getRank().getNumber() - pairs.get(i).getRank().getNumber();
//            if (compared != 0) {
//                return compared;
//            }
//        }
//        // 比较高牌
//        return other.getHighCard().getRankNumber() - highCard.getRankNumber();
//    }
}
