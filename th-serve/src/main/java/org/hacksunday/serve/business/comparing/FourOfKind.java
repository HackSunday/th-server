package org.hacksunday.serve.business.comparing;

import lombok.Getter;
import org.hacksunday.serve.vo.Card;
import org.hacksunday.serve.vo.HandValueRankEnum;

import java.util.List;

@Getter
public class FourOfKind implements HandValue {

    private List<Card> cards;

    private List<Card> four;

    private Card high;

    private int rankScore;

    FourOfKind(List<Card> cards, List<Card> four, Card high) {
        this.cards = cards;
        this.four = four;
        this.high = high;
        calcRankScore();
    }


    private void calcRankScore() {
        String hex = Integer.toHexString(four.get(0).getRankNumber()) +
                Integer.toHexString(high.getRankNumber());
        this.rankScore = getRank().getRankNum() + Integer.parseInt(hex, 16);
    }

    @Override
    public HandValueRankEnum getRank() {
        return HandValueRankEnum.FOUR_KIND;
    }

    @Override
    public List<Card> getCards() {
        return cards;
    }

//    @Override
//    public int sameRankCompare(HandValue o) {
//        FourOfKind other = (FourOfKind) o;
//        // 先比较四条的大小，再比较高牌
//        int compareFour = other.getFour().get(0).getRankNumber() - four.get(0).getRank().getNumber();
//        if (compareFour != 0) {
//            return compareFour;
//        } else {
//            return other.getHigh().getRankNumber() - high.getRankNumber();
//        }
//    }
}
