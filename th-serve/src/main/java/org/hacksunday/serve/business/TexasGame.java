package org.hacksunday.serve.business;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.hacksunday.lib.GrpcRoundEnum;
import org.hacksunday.serve.business.comparing.HandValue;
import org.hacksunday.serve.business.comparing.HandValueFactory;
import org.hacksunday.serve.service.RuntimeManager;
import org.hacksunday.serve.vo.*;

import java.util.*;

@Setter
@Getter
@Slf4j
public class TexasGame {

    private TexasGameConfig texasGameConfig;

    private GrpcRoundEnum roundEnum = GrpcRoundEnum.NOT_START;

    /**
     * 本局的扑克牌
     */
    private Poker poker = new Poker();

    /**
     * 在进行游戏的玩家
     */
    private Map<Integer, TexasPlayer> texasPlayers = new HashMap<>();

    /**
     * 公共牌
     */
    private List<Card> cardPool = new ArrayList<>();

    /**
     * 注池
     */
    private ChipsPool chipsPool;

    // 游戏状态信息
    /**
     * 庄家位置
     */
    private int dealerPos;

    /**
     * 当前操作位
     */
    private int currentPos;

    /**
     * 当前玩家剩余操作时间
     */
    private int remainSeconds;

    /**
     * 当前轮可以操作的玩家数
     */
    private int canOpCount;

    /**
     * 当前轮已经操作的玩家数
     */
    private int operatedCount = 0;


    public TexasPlayer getCurrentPlayer() {
        return texasPlayers.get(currentPos);
    }


    public void nextRound() {
        switch (roundEnum) {
            case PRE_FLOP:
                flop();
                break;
            case FLOP:
                turn();
                break;
            case TURN:
                river();
                break;
            case RIVER:
                showDown();
                break;
        }
    }

    private void showDown() {
        // fixme 目前show down阶段无操作，直接跳转到游戏结束阶段进行结算，后续可以增加玩家自行翻牌或者muck的feature
        // 当前为show_down阶段时，所有未fold玩家会自动翻牌
        roundEnum = GrpcRoundEnum.SHOW_DOWN;
        RuntimeManager.notifyGame(this);
    }

    private void river() {
        roundEnum = GrpcRoundEnum.RIVER;
        // 销一张牌
        burnCard();
        // 翻一张公共牌
        cardPool.add(poker.dispatch());
        if (needOp()) {
            // 从庄家的下一位开始喊注
            setCurrentPos(dealerPos);
            moveNextOpPos();
            newBetRound();
            RuntimeManager.notifyGame(this);
        }
    }

    private void turn() {
        roundEnum = GrpcRoundEnum.TURN;
        // 销一张牌
        burnCard();
        // 翻一张公共牌
        cardPool.add(poker.dispatch());
        if (needOp()) {
            // 从庄家的下一位开始喊注
            setCurrentPos(dealerPos);
            int next = moveNextOpPos();
            newBetRound();
            RuntimeManager.notifyGame(this);
        }
    }

    private void flop() {
        roundEnum = GrpcRoundEnum.FLOP;
        // 销一张牌
        burnCard();
        // 翻3张公共牌
        for (int i = 0; i < 3; i++) {
            cardPool.add(poker.dispatch());
        }
        if (needOp()) {
            // 从庄家的下一位开始喊注
            setCurrentPos(dealerPos);
            int next = moveNextOpPos();
            newBetRound();
            RuntimeManager.notifyGame(this);
        }
    }

    public void preFlop() {
        newBetRound();
        setCurrentPos(dealerPos);
        // 小盲下注
        int sbPos = moveNextOpPos();
        chipsPool.putChips(sbPos, texasGameConfig.getSmallBlind());
        // 大盲下注
        int bbPos = moveNextOpPos();
        chipsPool.putChips(bbPos, getTexasGameConfig().getSmallBlind() * 2);
        // 从小盲位开始发牌
        setCurrentPos(dealerPos);
        moveNextOpPos();
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < getTexasPlayers().size(); j++) {
                TexasPlayer player = getCurrentPlayer();
                dispatch(player);
                moveNextOpPos();
            }
        }
        // 移动操作位到大盲的下一位
        setCurrentPos(dealerPos);
        moveNextOpPos();
        moveNextOpPos();
        moveNextOpPos();

        RuntimeManager.notifyGame(this);
    }


    /**
     * 移动到下一个可操作（未fold和all in）位置
     *
     * @return 返回移动后的位置
     */
    public int moveNextOpPos() {
        if (!needOp()) {
            return currentPos;
        }
        if (roundOver() && leftOneCanOp()) {
            currentPos = -1;
            return currentPos;
        }
        for (int i = 0; i < texasGameConfig.getTotalChairCount(); i++) {
            int nextPos = currentPos + 1;
            currentPos = nextPos >= texasGameConfig.getTotalChairCount() ? nextPos % texasGameConfig.getTotalChairCount() : nextPos;
            TexasPlayer player = texasPlayers.get(currentPos);
            if (null != player && player.notFold() && !player.isAllIn()) {
                // 重置操作时间并返回
                remainSeconds = 30;
                return currentPos;
            }
        }
        currentPos = -1;
        return currentPos;
    }

    public TexasPlayer moveNextAndGet() {
        int nextPos = moveNextOpPos();
        return texasPlayers.get(nextPos);
    }

    /**
     * 发牌给玩家
     *
     * @param player
     */
    public void dispatch(TexasPlayer player) {
        if (player.notFold()) {
            player.getCards().add(poker.dispatch());
        }
    }

    /**
     * 弃牌
     */
    public void burnCard() {
        poker.dispatch();
    }


    /**
     * all in筹码
     *
     * @param userId
     */
    public void allIn(int userId) {
        int pos = getUserPos(userId);
        chipsPool.allIn(pos);
        this.operatedCount++;
        moveNextOpPos();
    }


    public void call(int userId) {
        int pos = getUserPos(userId);
        chipsPool.call(pos);
        this.operatedCount++;
        moveNextOpPos();
    }


    /**
     * 用户fold
     *
     * @param userId
     */
    public void fold(int userId) {
        int pos = getUserPos(userId);
        TexasPlayer player = texasPlayers.get(pos);
        player.setPlayingStatus(PlayingStatus.FOLD);
        this.operatedCount++;
        moveNextOpPos();
    }


    public void check(int userId) {
        this.operatedCount++;
        moveNextOpPos();
    }

    public void raise(int userId, Integer chips) {
        int pos = getUserPos(userId);
        chipsPool.raise(pos, chips);
        moveNextOpPos();
        // 有人raise之后重新喊一轮注
        this.operatedCount = 1;
    }

    public void shouldGoNextRound() {
        if (roundOver()) {
            nextRound();
        }
    }


    /**
     * 场上只有一位玩家或者游戏已结束
     *
     * @return 游戏是否结束
     */
    public boolean shouldOver() {
        // fixme 目前show down阶段无操作，直接跳转到游戏结束阶段进行结算，后续可以增加玩家自行翻牌或者muck的feature
        // 游戏进行到show_down 则结束
        // 剩余一位未fold玩家
        return roundEnum.equals(GrpcRoundEnum.SHOW_DOWN) || leftOneUnFold();
    }


    /**
     * 本轮结束
     *
     * @return
     */
    public boolean roundOver() {
        // 所有人已经喊注完成
        return operatedCount >= canOpCount;
    }

    /**
     * 只剩下一位未fold玩家
     *
     * @return
     */
    public boolean leftOneUnFold() {
        // 未fold玩家只剩一个
        int unFoldCount = 0;
        for (TexasPlayer player : texasPlayers.values()) {
            if (player.notFold()) {
                unFoldCount++;
            }
        }
        return unFoldCount == 1;
    }

    public boolean needOp() {
        return currentPos >= 0;
    }

    public boolean leftOneCanOp() {
        return calcCanOpCount() <= 1;
    }

    public List<Integer> getUnfoldUserPos() {
        List<Integer> results = new ArrayList<>();
        for (Map.Entry<Integer, TexasPlayer> entry : texasPlayers.entrySet()) {
            if (entry.getValue().notFold()) {
                results.add(entry.getKey());
            }
        }
        return results;
    }


    /**
     * 进行结算
     */
    public List<PlayerSettlement> settle() {
        List<Integer> unfoldUserPos = getUnfoldUserPos();
        if (unfoldUserPos.size() == 1) {
            return fastSettle(unfoldUserPos.get(0));
        }
        // 牌型比较
        Map<Integer, HandValue> posHandValueMap = new HashMap<>();
        for (Integer pos : unfoldUserPos) {
            TexasPlayer texasPlayer = texasPlayers.get(pos);
            HandValue handValue = HandValueFactory.getHandValue(texasPlayer.getCards(), cardPool);
            posHandValueMap.put(pos, handValue);
        }
        log.info("牌型结算结果：{}", JSON.toJSONString(posHandValueMap));

        // 筹码结算
        return chipsPool.settle(posHandValueMap);
    }

    /**
     * 快速结算
     *
     * @param pos
     */
    public List<PlayerSettlement> fastSettle(int pos) {
        return Collections.singletonList(PlayerSettlement.builder()
                .chips(chipsPool.fastSettle(pos)).pos(pos).userId(texasPlayers.get(pos).getUserId()).build());
    }


    public int getUserPos(int userId) {
        for (Map.Entry<Integer, TexasPlayer> entry : texasPlayers.entrySet()) {
            if (entry.getValue().getUserId() == userId) {
                return entry.getKey();
            }
        }
        return -1;
    }

    public void newBetRound() {
        canOpCount = calcCanOpCount();
        operatedCount = 0;
        chipsPool.newBetRound();
    }

    private int calcCanOpCount() {
        // 未all in玩家少于等于一个
        int canOpCount = 0;
        for (TexasPlayer player : texasPlayers.values()) {
            if (!player.isAllIn() && player.notFold()) {
                canOpCount++;
            }
        }
        return canOpCount;
    }

}
