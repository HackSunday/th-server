package org.hacksunday.serve.business.comparing;

import lombok.Getter;
import org.hacksunday.serve.vo.Card;
import org.hacksunday.serve.vo.HandValueRankEnum;

import java.util.Collections;
import java.util.List;

@Getter
public class OnePair implements HandValue {

    private List<Card> pairs;

    private List<Card> highCards;

    private List<Card> cards;

    private int rankScore;

    public OnePair(List<Card> pairs, List<Card> highCards, List<Card> cards) {
        this.pairs = pairs;
        this.highCards = highCards;
        this.cards = cards;
        calcRankScore();
    }


    private void calcRankScore() {
        StringBuilder hex = new StringBuilder();
        hex.append(pairs.get(0));
        for (Card card : highCards) {
            hex.append(Integer.toHexString(card.getRankNumber()));
        }
        this.rankScore = getRank().getRankNum() + Integer.parseInt(hex.toString(), 16);
    }

    @Override
    public HandValueRankEnum getRank() {
        return HandValueRankEnum.PAIR;
    }

    @Override
    public List<Card> getCards() {
        return cards;
    }

    @Override
    public int getRankScore() {
        return rankScore;
    }

//    @Override
//    public int sameRankCompare(HandValue o) {
//        OnePair other = (OnePair) o;
//        Collections.sort(other.getPairs());
//        Collections.sort(pairs);
//        // 先比较对子大小
//        for (int i = 0; i < pairs.size(); i++) {
//            int compared = other.getPairs().get(i).getRank().getNumber() - pairs.get(i).getRank().getNumber();
//            if (compared != 0) {
//                return compared;
//            }
//        }
//        // 比较高牌
//        for (int i = 0; i < cards.size(); i++) {
//            int compared = other.getHighCards().get(i).getRank().getNumber() - highCards.get(i).getRank().getNumber();
//            if (compared != 0) {
//                return compared;
//            }
//        }
//        return 0;
//    }

}
