package org.hacksunday.serve.business.comparing;

import lombok.Getter;
import org.hacksunday.serve.vo.Card;
import org.hacksunday.serve.vo.HandValueRankEnum;

import java.util.List;

@Getter
public class ThreeOfKind implements HandValue {

    private List<Card> three;

    private List<Card> highCards;

    private List<Card> cards;

    private int rankScore;

    public ThreeOfKind(List<Card> three, List<Card> highCards, List<Card> cards) {
        this.three = three;
        this.highCards = highCards;
        this.cards = cards;
        calcRankScore();
    }

    private void calcRankScore() {
        StringBuilder hex = new StringBuilder();
        hex.append(Integer.toHexString(three.get(0).getRankNumber()));
        for (Card card : highCards) {
            hex.append(Integer.toHexString(card.getRankNumber()));
        }
        this.rankScore = getRank().getRankNum() + Integer.parseInt(hex.toString(), 16);
    }

    @Override
    public HandValueRankEnum getRank() {
        return HandValueRankEnum.THREE_KIND;
    }

    @Override
    public List<Card> getCards() {
        return cards;
    }

//    @Override
//    public int sameRankCompare(HandValue o) {
//        ThreeOfKind other = (ThreeOfKind) o;
//        // 先比较三条的大小
//        int compareThree = other.getThree().get(0).getRankNumber() - three.get(0).getRankNumber();
//        if (compareThree != 0) {
//            return compareThree;
//        }
//        // 三条相等比较高牌
//        else {
//            for (int i = 0; i < highCards.size(); i++) {
//                int compared = other.getHighCards().get(i).getRank().getNumber() - highCards.get(i).getRank().getNumber();
//                if (compared != 0) {
//                    return compared;
//                }
//            }
//        }
//        return 0;
//    }
}
