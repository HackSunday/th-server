package org.hacksunday.serve.business.comparing;

import lombok.Getter;
import org.hacksunday.serve.vo.Card;
import org.hacksunday.serve.vo.HandValueRankEnum;

import java.util.List;

@Getter
public class FullHouse implements HandValue {

    private List<Card> cards;

    private List<Card> three;

    private List<Card> two;

    private int rankScore;

    FullHouse(List<Card> cards, List<Card> three, List<Card> two) {
        this.cards = cards;
        this.three = three;
        this.two = two;
        calcRankScore();
    }

    private void calcRankScore() {
        String hex = Integer.toHexString(three.get(0).getRankNumber()) +
                Integer.toHexString(two.get(0).getRankNumber());
        this.rankScore = getRank().getRankNum() + Integer.parseInt(hex, 16);
    }


    @Override
    public HandValueRankEnum getRank() {
        return HandValueRankEnum.FULL_HOUSE;
    }

    @Override
    public List<Card> getCards() {
        return null;
    }

//    @Override
//    public int sameRankCompare(HandValue o) {
//        FullHouse other = (FullHouse) o;
//        // 先比较三条的大小
//        int compareThree = other.getThree().get(0).getRankNumber() - three.get(0).getRankNumber();
//        if (compareThree != 0) {
//            return compareThree;
//        }
//        // 三条相等比较两条
//        else {
//            return other.getTwo().get(0).getRankNumber() - two.get(0).getRankNumber();
//        }
//    }
}
