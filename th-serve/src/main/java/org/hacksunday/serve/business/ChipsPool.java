package org.hacksunday.serve.business;

import io.grpc.Status;
import lombok.Getter;
import org.hacksunday.serve.business.comparing.HandValue;
import org.hacksunday.serve.ex.BusinessException;
import org.hacksunday.serve.service.RuntimeManager;
import org.hacksunday.serve.vo.PlayerSettlement;
import org.hacksunday.serve.vo.PlayingStatus;
import org.hacksunday.serve.vo.TexasPlayer;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 筹码池
 * 用于奖金分配
 */
@Getter
public class ChipsPool {

    private Map<Integer, TexasPlayer> texasPlayers = new ConcurrentHashMap<>();

    private Map<Integer, Integer> posRoundPutChips = new ConcurrentHashMap<>();

    private final Set<Integer> allInPos = new HashSet<>();

    private final AtomicInteger totalChips = new AtomicInteger();

    private int roundChips = 0;

    private int dealerPos;

    private int smallBlind = 10;

    public ChipsPool() {
    }

    public ChipsPool(Map<Integer, TexasPlayer> texasPlayerMap) {
        this.texasPlayers = texasPlayerMap;
    }


    /**
     * 下注
     *
     * @param pos
     * @param chips
     */
    public void putChips(int pos, int chips) {
        if (chips < 0) {
            return;
        }
        TexasPlayer player = texasPlayers.get(pos);
        // 不是all in的情况下，筹码不得低于当前轮的下注额
        int userRoundChips = chips + posRoundPutChips.getOrDefault(pos, 0);
        if (chips > player.getRemainChips()) {
            throw new BusinessException(Status.RESOURCE_EXHAUSTED.withDescription("筹码不够"));
        }
        // all in
        if (chips == player.getRemainChips()) {
            // 当前pos allIn
            markAllIn(pos);
        } else if (userRoundChips < roundChips) {
            throw new BusinessException(Status.UNAVAILABLE.withDescription("筹码不得低于当前" + roundChips));
        }
        // 下注筹码大于roundChips
        if (userRoundChips > roundChips) {
            // 当前用户raise
            roundChips = userRoundChips;
        }
        // 记录每个位置这一轮投入的筹码
        posRoundPutChips.put(pos, userRoundChips);
        // 记录总筹码
        totalChips.addAndGet(chips);
        // 修改玩家的下注筹码和剩余筹码
        player.setPutinChips(player.getPutinChips() + chips);
        player.setRemainChips(player.getRemainChips() - chips);
    }

    public void call(int pos) {
        int chips = roundChips - posRoundPutChips.get(pos);
        if (chips > 0) {
            // all in
            if (chips == texasPlayers.get(pos).getRemainChips()) {
                allIn(pos);
            } else {
                putChips(pos, chips);
            }

        }
    }

    public void raise(int pos, Integer chips) {
        putChips(pos, chips);
    }


    public void allIn(int pos) {
        putChips(pos, texasPlayers.get(pos).getRemainChips());
        TexasPlayer player = texasPlayers.get(pos);
        player.setPlayingStatus(PlayingStatus.ALL_IN);
    }


    /**
     * 标记用户已经all in
     *
     * @param pos
     */
    public void markAllIn(int pos) {
        allInPos.add(pos);
    }

    public boolean canCheck(int pos) {
        return roundChips == 0 || posRoundPutChips.getOrDefault(pos, 0) >= roundChips;
    }

    public boolean canCall(int pos) {
        return roundChips > 0 && texasPlayers.get(pos).getRemainChips() + posRoundPutChips.getOrDefault(pos, 0) >= roundChips;
    }

    public boolean canRaise(int pos) {
        return texasPlayers.get(pos).getRemainChips() + posRoundPutChips.getOrDefault(pos, 0) > roundChips
                && !RuntimeManager.getGame(getPosUser(pos)).leftOneCanOp();
    }

    public boolean canAllIn(int pos) {
        return texasPlayers.get(pos).getRemainChips() > 0;
    }


    public void newBetRound() {
        roundChips = 0;
        posRoundPutChips = new ConcurrentHashMap<>();
    }

    /**
     * @param posHandValueMap <位置，HandValue> map
     * @return 返回每个用户应分得的筹码 按照牌型大小顺序
     */
    public List<PlayerSettlement> settle(Map<Integer, HandValue> posHandValueMap) {
        Map<Integer, PlayerSettlement> result = new HashMap<>();
        // 构造 <牌型分,List<pos>>的map用于计算
        Map<Integer, List<Integer>> rankNumPosMap = new HashMap<>();
        for (Map.Entry<Integer, HandValue> entry : posHandValueMap.entrySet()) {
            HandValue handValue = entry.getValue();
            Integer pos = entry.getKey();
            rankNumPosMap.putIfAbsent(handValue.getRankScore(), new ArrayList<>());
            rankNumPosMap.get(handValue.getRankScore()).add(pos);
            result.put(pos, PlayerSettlement.builder().pos(pos).userId(texasPlayers.get(pos).getUserId()).rankEnum(handValue.getRank()).rankScore(handValue.getRankScore()).build());
        }
        // 按照rank score来从大到小计算获得的筹码
        doSettle(result, rankNumPosMap);

        List<PlayerSettlement> settlements = new ArrayList<>(result.values());
        settlements.sort(((o1, o2) -> o2.getRankScore() - o1.getRankScore()));
        return settlements;
    }

    private void doSettle(Map<Integer, PlayerSettlement> result, Map<Integer, List<Integer>> rankNumPosMap) {
        // 按照rank score来从大到小计算获得的筹码
        List<Integer> rankScoreList = new ArrayList<>(rankNumPosMap.keySet());
        rankScoreList.sort((o1, o2) -> o2 - o1);
        int sidePool = 0;
        Map<Integer, TexasPlayer> copy = new HashMap<>(texasPlayers);
        for (Integer rankNum : rankScoreList) {
            List<Integer> posList = rankNumPosMap.get(rankNum);
            // 按照下注由少到多排序
            posList.sort(Comparator.comparingInt(o -> copy.get(o).getPutinChips()));
            while (!CollectionUtils.isEmpty(posList)) {
                // 计算当前池子奖金额
                int pos = posList.remove(0);
                int winChipsTotal = 0;
                int winnerChips = copy.get(pos).getPutinChips();
                // 玩家能获得的筹码为 下注金额 - 已分走的
                int winChips = winnerChips - sidePool;
                if (winChips <= 0) {
                    continue;
                }
                for (TexasPlayer value : copy.values()) {
                    int putInChips = value.getPutinChips();
                    // 剩余可分配的筹码为 下注金额 - 已分走的
                    int remainChips = putInChips - sidePool;
                    if (remainChips > 0) {
                        // 赢家可分得金额为
                        winChipsTotal += Math.min(remainChips, winChips);
                    }
                }
                sidePool = winnerChips;
                // 均分
                List<Integer> winPosList = new ArrayList<>(posList);
                winPosList.add(pos);
                distributeChips(result, winPosList, winChipsTotal);
                // 移除已经分配过的玩家
                copy.remove(pos);
            }
        }
    }

    private void distributeChips(Map<Integer, PlayerSettlement> result, List<Integer> winPosList, int winChipsTotal) {
        // 只有一位玩家直接快速分配全额
        if (winPosList.size() == 1) {
            Integer first = winPosList.get(0);
            result.get(first).addChips(winChipsTotal);
            return;
        }
        int sbCount = winChipsTotal / winPosList.size() / smallBlind;
        int mod = winChipsTotal / smallBlind % winPosList.size();
        int nearestPos = winPosList.get(0);
        for (Integer winPos : winPosList) {
            result.get(winPos).addChips(sbCount * smallBlind);
            // 无法均分，计算哪个位置离庄家最近
            if (mod > 0) {
                if (nearestPos > dealerPos) {
                    if (winPos > dealerPos && winPos < nearestPos) {
                        nearestPos = winPos;
                    }
                } else {
                    if (winPos > dealerPos) {
                        nearestPos = winPos;
                    } else if (winPos < nearestPos) {
                        nearestPos = winPos;
                    }
                }
            }
        }
        // 将额外的筹码给顺时针离庄家最近的玩家
        if (mod > 0) {
            result.get(nearestPos).addChips(mod * smallBlind);
        }
    }

    /**
     * 只有一位玩家获胜的情况，直接快速结算 赢走全部筹码
     *
     * @param pos
     * @return
     */
    public int fastSettle(int pos) {
        return totalChips.get();
    }

    public void setDealerPos(int dealerPos) {
        this.dealerPos = dealerPos;
    }

    public void setSmallBlind(int smallBlind) {
        this.smallBlind = smallBlind;
    }

    private int getPosUser(int pos) {
        for (Map.Entry<Integer, TexasPlayer> entry : texasPlayers.entrySet()) {
            if (entry.getKey() == pos) {
                return entry.getValue().getUserId();
            }
        }
        return -1;
    }
}
