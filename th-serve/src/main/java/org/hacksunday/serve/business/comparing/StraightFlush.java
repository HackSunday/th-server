package org.hacksunday.serve.business.comparing;

import org.hacksunday.serve.vo.Card;
import org.hacksunday.serve.vo.HandValueRankEnum;

import java.util.List;

public class StraightFlush implements HandValue {

    private List<Card> cards;

    private int rankScore;

    StraightFlush(List<Card> cards) {
        this.cards = cards;
        calcRankScore();
    }


    private void calcRankScore() {
        this.rankScore = getRank().getRankNum() + Integer.parseInt(Integer.toHexString(cards.get(1).getRankNumber()), 16);
    }

    @Override
    public HandValueRankEnum getRank() {
        return HandValueRankEnum.STRAIGHT_FLUSH;
    }

    @Override
    public List<Card> getCards() {
        return cards;
    }

    @Override
    public int getRankScore() {
        return rankScore;
    }

//    @Override
//    public int sameRankCompare(HandValue o) {
//        // cards已按大-->小排列，为了避免A 2 3 4 5 和5 6 7 8 9这两种顺子，比较第二位即可
//        return o.getCards().get(1).getRank().getNumber() - cards.get(1).getRank().getNumber();
//    }
}
