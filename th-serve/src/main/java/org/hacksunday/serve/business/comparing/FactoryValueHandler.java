package org.hacksunday.serve.business.comparing;

import org.hacksunday.serve.vo.Card;
import org.springframework.util.CollectionUtils;

import java.util.List;

public abstract class FactoryValueHandler {

    protected List<Card> selectedCards;

    public FactoryValueHandler() {
    }

    public boolean match() {
        return !CollectionUtils.isEmpty(selectedCards);
    }

    public void setSelectedCards(List<Card> selectedCards) {
        this.selectedCards = selectedCards;
    }

    public List<Card> getSelectedCards() {
        return selectedCards;
    }

    public abstract HandValue getHandValue();

    protected abstract void selectCard();
}
