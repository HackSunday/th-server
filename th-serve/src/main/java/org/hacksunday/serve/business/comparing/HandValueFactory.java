package org.hacksunday.serve.business.comparing;

import org.hacksunday.serve.vo.Card;

import java.util.List;

public class HandValueFactory {

    public static HandValue getHandValue(List<Card> handCards, List<Card> poolCards) {
        FactoryHandValue factoryHandValue = new FactoryHandValue(handCards, poolCards);
        return factoryHandValue.getHandValue();
    }
}
