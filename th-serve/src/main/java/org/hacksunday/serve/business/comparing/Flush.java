package org.hacksunday.serve.business.comparing;

import org.hacksunday.serve.vo.Card;
import org.hacksunday.serve.vo.HandValueRankEnum;

import java.util.List;

public class Flush implements HandValue {

    private List<Card> cards;

    private int rankScore;

    public Flush(List<Card> cards) {
        this.cards = cards;
        calcRankScore();
    }

    private void calcRankScore() {
        StringBuilder hex = new StringBuilder();
        for (Card card : cards) {
            hex.append(Integer.toHexString(card.getRankNumber()));
        }
        this.rankScore = getRank().getRankNum() + Integer.parseInt(hex.toString(), 16);
    }

    @Override
    public HandValueRankEnum getRank() {
        return HandValueRankEnum.FLUSH;
    }

    @Override
    public List<Card> getCards() {
        return cards;
    }

    @Override
    public int getRankScore() {
        return rankScore;
    }

//    @Override
//    public int sameRankCompare(HandValue o) {
//        // 两副高牌逐一比较
//        for (int i = 0; i < cards.size(); i++) {
//            int compared = o.getCards().get(i).getRank().getNumber() - cards.get(i).getRank().getNumber();
//            if (compared != 0) {
//                return compared;
//            }
//        }
//        return 0;
//    }

}
