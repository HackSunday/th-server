package org.hacksunday.serve.business.comparing;

import org.hacksunday.serve.vo.Card;
import org.hacksunday.serve.vo.HandValueRankEnum;

import java.util.List;

/**
 * 牌型大小比较
 */
public interface HandValue extends Comparable<HandValue> {

    HandValueRankEnum getRank();

    List<Card> getCards();

    int getRankScore();

    @Override
    default int compareTo(HandValue o) {
        return o.getRankScore() - getRankScore();
    }

    /**
     * 相同rank 降序比较
     * @param o
     * @return
     */
//    int sameRankCompare(HandValue o);
}
