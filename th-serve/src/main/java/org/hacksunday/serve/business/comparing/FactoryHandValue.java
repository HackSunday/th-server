package org.hacksunday.serve.business.comparing;

import com.google.common.collect.Lists;
import lombok.Getter;
import org.hacksunday.serve.vo.Card;
import org.hacksunday.serve.vo.CardRankEnum;
import org.hacksunday.serve.vo.CardSuitEnum;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Getter
public class FactoryHandValue {
    /**
     * unmodified
     */
    private final List<Card> handCards;

    /**
     * unmodified
     */
    private final List<Card> poolCards;

    /**
     * unmodified
     */
    private final List<Card> allCards;

    /**
     * 同花的牌
     * unmodified
     */
    private List<Card> flushCards;

    /**
     * 顺子的牌
     * unmodified
     */
    private List<Card> straightCards;

    /**
     * 相同点数的牌
     */
    private Map<CardRankEnum, List<Card>> sameRankCards;

    private List<FactoryValueHandler> valueHandlers = new ArrayList<>();

    private CardSuitEnum flushSuit;

    private HandValue handValue;


    FactoryHandValue(List<Card> handCards, List<Card> poolCards) {
        this.handCards = Collections.unmodifiableList(handCards);
        this.poolCards = Collections.unmodifiableList(poolCards);
        ArrayList<Card> allCards = new ArrayList<>(handCards);
        allCards.addAll(poolCards);
        this.allCards = Collections.unmodifiableList(allCards);
        // 处理同花
        handleFlushSuit();
        // 处理顺子
        this.straightCards = Collections.unmodifiableList(handleStraight(allCards));
        // 处理相同点数牌
        handleSameRank();
        // 按照牌型从大到小处理
        initFactoryValueHandler();
        handleHandValue();
    }


    private void initFactoryValueHandler() {
        // 皇家同花顺
        valueHandlers.add(new RoyalStraightFlushHandler());
        // 同花顺
        valueHandlers.add(new StraightFlushHandler());
        // 四条
        valueHandlers.add(new FourOfKindHandler());
        // 葫芦
        valueHandlers.add(new FullHouseHandler());
        // 同花
        valueHandlers.add(new FlushHandler());
        // 顺子
        valueHandlers.add(new StraightHandler());
        // 三条
        valueHandlers.add(new ThreeOfKindHandler());
        // 两对
        valueHandlers.add(new TwoPairHandler());
        // 一对
        valueHandlers.add(new OnePairHandler());
        // 高牌
        valueHandlers.add(new HighCardHandler());
    }


    public HandValue getHandValue() {
        return handValue;
    }

    private void handleHandValue() {
        for (FactoryValueHandler valueHandler : valueHandlers) {
            valueHandler.selectCard();
            if (valueHandler.match()) {
                this.handValue = valueHandler.getHandValue();
                break;
            }
        }
    }

    private void handleSameRank() {
        sameRankCards = new HashMap<>();
        for (Card card : allCards) {
            sameRankCards.putIfAbsent(card.getRank(), new ArrayList<>());
            sameRankCards.get(card.getRank()).add(card);
        }
    }

    /**
     * 处理顺子牌型
     * 注意ace的处理 Ace可以为1也可以为14
     */
    private List<Card> handleStraight(List<Card> cardList) {
        Map<CardRankEnum, List<Card>> cardMap = new HashMap<>();
        for (Card card : cardList) {
            cardMap.putIfAbsent(card.getRank(), new ArrayList<>());
            cardMap.get(card.getRank()).add(card);
        }
        int count = 0;
        List<Card> straightList = new ArrayList<>();
        // ace的特殊处理
        List<CardRankEnum> all = Lists.newArrayList(CardRankEnum.values());
        all.add(0, CardRankEnum.CARD_ACE);
        for (CardRankEnum rankEnum : all) {
            if (cardMap.containsKey(rankEnum)) {
                count++;
                straightList.addAll(cardMap.get(rankEnum));
            } else {
                if (count < 5) {
                    count = 0;
                    straightList = new ArrayList<>();
                } else {
                    return straightList;
                }
            }
        }
        return straightList;
    }

    /**
     * 处理同花
     */
    private void handleFlushSuit() {
        Map<CardSuitEnum, List<Card>> suitCards = new HashMap<>();
        // 按照相同花色构造map
        for (Card card : allCards) {
            suitCards.putIfAbsent(card.getSuit(), new ArrayList<>());
            suitCards.get(card.getSuit()).add(card);
        }
        for (Map.Entry<CardSuitEnum, List<Card>> entry : suitCards.entrySet()) {
            if (entry.getValue().size() >= 5) {
                flushCards = entry.getValue();
                flushSuit = entry.getKey();
                break;
            }
        }
    }

    protected boolean hasFlush() {
        return null != flushSuit;
    }

    protected boolean hasStraight() {
        return !CollectionUtils.isEmpty(straightCards) && straightCards.size() >= 5;
    }

    class RoyalStraightFlushHandler extends FactoryValueHandler {

        @Override
        public HandValue getHandValue() {
            return new RoyalStraightFlush(getSelectedCards());
        }

        @Override
        protected void selectCard() {
            // 牌型含有顺子和同花
            if (hasFlush() && hasStraight()) {
                List<Card> cards = new ArrayList<>(getFlushCards());
                // 同时在顺子和同花的卡牌
                cards.retainAll(handleStraight(cards));
                if (cards.size() >= 5) {
                    // 排序
                    Collections.sort(cards);
                    // 第二张为king
                    if (cards.get(1).getRank().equals(CardRankEnum.CARD_KING)) {
                        setSelectedCards(cards.subList(0, 5));
                    }
                }
            }
        }
    }


    class StraightFlushHandler extends FactoryValueHandler {

        @Override
        public HandValue getHandValue() {
            return new StraightFlush(getSelectedCards());
        }

        @Override
        protected void selectCard() {
            // 牌型含有顺子和同花
            if (hasFlush() && hasStraight()) {
                List<Card> cards = new ArrayList<>(getFlushCards());
                // 同时在顺子和同花的卡牌
                cards.retainAll(handleStraight(cards));
                if (cards.size() >= 5) {
                    // 排序
                    Collections.sort(cards);
                    // 特殊处理A
                    if (cards.get(0).getRank().equals(CardRankEnum.CARD_ACE)) {
                        // ace移到队尾
                        Card remove = cards.remove(0);
                        cards.add(remove);
                    }
                    setSelectedCards(cards.subList(0, 5));
                }
            }
        }
    }

    class FourOfKindHandler extends FactoryValueHandler {

        private List<Card> four = new ArrayList<>();

        private Card high;

        @Override
        public HandValue getHandValue() {
            return new FourOfKind(getSelectedCards(), four, high);
        }

        @Override
        protected void selectCard() {
            for (Map.Entry<CardRankEnum, List<Card>> entry : getSameRankCards().entrySet()) {
                if (entry.getValue().size() >= 4) {
                    four.addAll(entry.getValue());
                    List<Card> allCards = new ArrayList<>(getAllCards());
                    // 移除4条的牌之后再选择一个高牌
                    allCards.removeAll(four);
                    Collections.sort(allCards);
                    high = allCards.get(0);
                    List<Card> cards = new ArrayList<>(four);
                    cards.add(high);
                    setSelectedCards(Collections.unmodifiableList(cards));
                }
            }
        }
    }

    class FullHouseHandler extends FactoryValueHandler {

        private List<Card> three;

        private List<Card> two;

        @Override
        public HandValue getHandValue() {
            return new FullHouse(getSelectedCards(), three, two);
        }

        @Override
        protected void selectCard() {
            List<CardRankEnum> twoRank = new ArrayList<>();
            List<CardRankEnum> threeRank = new ArrayList<>();
            // 手牌加公共牌可能出现 aaabbbc和aaabbcc的情况，选牌时要选择最大的
            for (Map.Entry<CardRankEnum, List<Card>> entry : getSameRankCards().entrySet()) {
                if (entry.getValue().size() == 3) {
                    threeRank.add(entry.getKey());
                } else if (entry.getValue().size() == 2) {
                    twoRank.add(entry.getKey());
                }
            }
            if (!CollectionUtils.isEmpty(threeRank) && threeRank.size() + twoRank.size() > 1) {
                // aaabbbc的情况
                if (threeRank.size() > 1) {
                    CardRankEnum maxThreeRank = threeRank.stream().max(Comparator.comparingInt(CardRankEnum::getNumber)).get();
                    this.three = getSameRankCards().get(maxThreeRank);
                    threeRank.remove(maxThreeRank);
                    this.two = getSameRankCards().get(threeRank.get(0)).subList(0, 2);
                } else {
                    this.three = getSameRankCards().get(threeRank.get(0));
                    // aaabbcc的情况
                    if (twoRank.size() > 1) {
                        CardRankEnum maxTwo = twoRank.stream().max(Comparator.comparingInt(CardRankEnum::getNumber)).get();
                        this.two = getSameRankCards().get(maxTwo);
                    } else {
                        this.two = getSameRankCards().get(twoRank.get(0));
                    }
                }
                List<Card> selected = new ArrayList<>(this.three);
                selected.addAll(this.two);
                setSelectedCards(selected);
            }
        }
    }

    class FlushHandler extends FactoryValueHandler {

        @Override
        public HandValue getHandValue() {
            return new Flush(getSelectedCards());
        }

        @Override
        protected void selectCard() {
            if (hasFlush()) {
                List<Card> selected = new ArrayList<>(flushCards);
                Collections.sort(selected);
                setSelectedCards(selected.subList(0, 5));
            }
        }
    }

    class StraightHandler extends FactoryValueHandler {

        @Override
        public HandValue getHandValue() {
            return new Straight(getSelectedCards());
        }

        @Override
        protected void selectCard() {
            if (hasStraight()) {
                List<Card> selected = new ArrayList<>(straightCards);
                Collections.sort(selected);
                // 特殊处理A
                if (selected.get(0).getRank().equals(CardRankEnum.CARD_ACE) && !selected.get(1).getRank().equals(CardRankEnum.CARD_KING)) {
                    // ace移到队尾
                    Card remove = selected.remove(0);
                    selected.add(remove);
                }
                setSelectedCards(selected.subList(0, 5));
            }
        }
    }

    class ThreeOfKindHandler extends FactoryValueHandler {

        List<Card> three = new ArrayList<>();

        List<Card> highCards = new ArrayList<>();

        @Override
        public HandValue getHandValue() {
            return new ThreeOfKind(three, highCards, getSelectedCards());
        }

        @Override
        protected void selectCard() {
            for (Map.Entry<CardRankEnum, List<Card>> entry : getSameRankCards().entrySet()) {
                if (entry.getValue().size() == 3) {
                    three.addAll(entry.getValue());
                } else {
                    highCards.addAll(entry.getValue());
                }
            }
            if (!CollectionUtils.isEmpty(three)) {
                Collections.sort(highCards);
                highCards = highCards.subList(0, 2);
                List<Card> selected = new ArrayList<>(three);
                selected.addAll(highCards);
                setSelectedCards(selected);
            }
        }
    }

    class TwoPairHandler extends FactoryValueHandler {

        private List<Card> pairs = new ArrayList<>();

        private Card highCard;

        @Override
        public HandValue getHandValue() {
            return new TwoPair(pairs, highCard, getSelectedCards());
        }

        @Override
        protected void selectCard() {
            // 可能存在aabbccd的情况，需要选择最大的两对
            List<CardRankEnum> twoRank = new ArrayList<>();
            for (Map.Entry<CardRankEnum, List<Card>> entry : getSameRankCards().entrySet()) {
                if (entry.getValue().size() == 2) {
                    twoRank.add(entry.getKey());
                }
            }
            if (twoRank.size() > 1) {
                // aabbccd的情况
                if (twoRank.size() > 2) {
                    twoRank.sort((o1, o2) -> o2.getNumber() - o1.getNumber());
                    twoRank = twoRank.subList(0, 2);
                }
                for (CardRankEnum rankEnum : twoRank) {
                    pairs.addAll(getSameRankCards().get(rankEnum));
                }
                List<Card> allCards = new ArrayList<>(getAllCards());
                allCards.removeAll(pairs);
                Collections.sort(allCards);
                this.highCard = allCards.get(0);
                List<Card> selected = new ArrayList<>(pairs);
                selected.add(highCard);
                setSelectedCards(selected);
            }
        }
    }

    class OnePairHandler extends FactoryValueHandler {

        private List<Card> pairs = new ArrayList<>();

        private List<Card> highCards = new ArrayList<>();


        @Override
        public HandValue getHandValue() {
            return new OnePair(pairs, highCards, getSelectedCards());
        }

        @Override
        protected void selectCard() {
            for (Map.Entry<CardRankEnum, List<Card>> entry : getSameRankCards().entrySet()) {
                if (entry.getValue().size() == 2) {
                    pairs.addAll(entry.getValue());
                }
            }
            if (!CollectionUtils.isEmpty(pairs)) {
                List<Card> allCards = new ArrayList<>(getAllCards());
                allCards.removeAll(pairs);
                Collections.sort(allCards);
                this.highCards = allCards.subList(0, 3);
                List<Card> selected = new ArrayList<>(pairs);
                selected.addAll(highCards);
                setSelectedCards(selected);
            }
        }
    }

    class HighCardHandler extends FactoryValueHandler {

        @Override
        public HandValue getHandValue() {
            return new HighCard(getSelectedCards());
        }

        @Override
        protected void selectCard() {
            List<Card> allCards = new ArrayList<>(getAllCards());
            Collections.sort(allCards);
            setSelectedCards(allCards.subList(0, 5));
        }
    }

}
