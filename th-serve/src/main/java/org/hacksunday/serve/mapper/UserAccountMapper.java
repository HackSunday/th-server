package org.hacksunday.serve.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.hacksunday.serve.domain.UserAccountDO;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author onefish
 * @since 2021-07-05
 */
public interface UserAccountMapper extends BaseMapper<UserAccountDO> {

    @Update("update user_account set coins = coins + #{winChips} where id = #{id}")
    Integer addChips(@Param("id") int id, @Param("winChips") int winChips);
}
