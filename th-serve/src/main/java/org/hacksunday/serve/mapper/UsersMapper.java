package org.hacksunday.serve.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.hacksunday.serve.domain.UserDO;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author onefish
 * @since 2021-07-05
 */
public interface UsersMapper extends BaseMapper<UserDO> {

}
