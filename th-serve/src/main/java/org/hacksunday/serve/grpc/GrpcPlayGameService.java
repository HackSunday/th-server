package org.hacksunday.serve.grpc;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.server.service.GrpcService;
import org.hacksunday.lib.*;
import org.hacksunday.serve.constant.Constants;
import org.hacksunday.serve.service.RuntimeManager;
import org.hacksunday.serve.service.TexasGameService;
import org.hacksunday.serve.service.TexasRoomService;

import javax.annotation.Resource;

@GrpcService(interceptorNames = "auth")
@Slf4j
public class GrpcPlayGameService extends PlayGameGrpc.PlayGameImplBase {

    @Resource
    TexasGameService texasGameService;

    @Resource
    TexasRoomService texasRoomService;


    @Override
    public StreamObserver<GrpcRoomCommand> room(StreamObserver<GrpcRoomReply> responseObserver) {
        int userId = Integer.parseInt(Constants.USER_ID_CONTEXT_KEY.get());
        RuntimeManager.putRoomObserver(userId, responseObserver);
        log.info("user: {} connect room", userId);
        return new StreamObserver<>() {
            @Override
            public void onNext(GrpcRoomCommand command) {
                texasRoomService.handleCommand(command, responseObserver);
            }

            @Override
            public void onError(Throwable throwable) {
                log.info("on error", throwable);
            }

            @Override
            public void onCompleted() {
                String userId = Constants.USER_ID_CONTEXT_KEY.get();
                log.info("{} completed", userId);
            }
        };
    }

    @Override
    public StreamObserver<GrpcGameCommand> play(StreamObserver<GrpcGameReply> responseObserver) {
        int userId = Integer.parseInt(Constants.USER_ID_CONTEXT_KEY.get());
        RuntimeManager.putGameObserver(userId, responseObserver);
        log.info("user: {} connect play", userId);
        return new StreamObserver<>() {
            @Override
            public void onNext(GrpcGameCommand gameCommand) {
                texasGameService.handleCommand(gameCommand, responseObserver);
            }

            @Override
            public void onError(Throwable throwable) {
                log.info("on error", throwable);
            }

            @Override
            public void onCompleted() {
                String userId = Constants.USER_ID_CONTEXT_KEY.get();
                log.info("{} completed", userId);
            }
        };
    }
}
