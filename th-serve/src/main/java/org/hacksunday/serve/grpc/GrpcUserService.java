package org.hacksunday.serve.grpc;

import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.advice.GrpcAdvice;
import net.devh.boot.grpc.server.service.GrpcService;
import org.hacksunday.lib.*;
import org.hacksunday.serve.service.UserService;

import javax.annotation.Resource;

@GrpcService
@GrpcAdvice
public class GrpcUserService extends UserServiceGrpc.UserServiceImplBase {

    @Resource
    UserService userService;

    @Override
    public void login(LoginRequest request, StreamObserver<LoginResponseVo> responseObserver) {
        LoginResponseVo response = userService.login(request);
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void signup(SignUpRequest request, StreamObserver<SignUpResponseVo> responseObserver) {
        SignUpResponseVo response = userService.signUp(request);
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
