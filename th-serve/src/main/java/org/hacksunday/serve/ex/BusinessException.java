package org.hacksunday.serve.ex;

import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;

import javax.annotation.Nullable;

public class BusinessException extends StatusRuntimeException {

    public BusinessException(Status status) {
        super(status);
    }

    public BusinessException(Status status, @Nullable Metadata trailers) {
        super(status, trailers);
    }
}
