package org.hacksunday.serve.constant;

import io.grpc.Context;
import io.grpc.Metadata;

import static io.grpc.Metadata.ASCII_STRING_MARSHALLER;

public class Constants {
    public static final String JWT_SIGNING_KEY = "5WhZA+kq5znX7eOKmg9OLvh23xqeMoPkFX+IiOg+H4Y=";
    public static final String BEARER_TYPE = "Bearer";

    public static final Metadata.Key<String> AUTHORIZATION_METADATA_KEY = Metadata.Key.of("Authorization", ASCII_STRING_MARSHALLER);
    public static final Context.Key<String> USER_ID_CONTEXT_KEY = Context.key("userId");

    private Constants() {
        throw new AssertionError();
    }
}