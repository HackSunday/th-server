package org.hacksunday.serve.interceptor;

import io.grpc.*;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import org.hacksunday.serve.constant.Constants;
import org.hacksunday.serve.service.PlayerService;
import org.hacksunday.serve.service.RuntimeManager;
import org.hacksunday.serve.service.UserService;
import org.hacksunday.serve.utils.JwtUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component("auth")
public class AuthorizationServerInterceptor implements ServerInterceptor {

    @Resource
    PlayerService playerService;

    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> serverCall, Metadata metadata, ServerCallHandler<ReqT, RespT> serverCallHandler) {
        String value = metadata.get(Constants.AUTHORIZATION_METADATA_KEY);

        Status status;
        if (value == null) {
            status = Status.UNAUTHENTICATED.withDescription("Authorization token is missing");
        } else if (!value.startsWith(Constants.BEARER_TYPE)) {
            status = Status.UNAUTHENTICATED.withDescription("Unknown authorization type");
        } else {
            try {
                String token = value.substring(Constants.BEARER_TYPE.length()).trim();
                Jws<Claims> claims = JwtUtils.parseClaimsJws(token);
                String subject = claims.getBody().getSubject();
                Context ctx = Context.current().withValue(Constants.USER_ID_CONTEXT_KEY, subject);
                // 保存用户session
                playerService.connect(Integer.parseInt(subject));
                return Contexts.interceptCall(ctx, serverCall, metadata, serverCallHandler);
            } catch (Exception e) {
                status = Status.UNAUTHENTICATED.withDescription(e.getMessage()).withCause(e);
            }
        }

        serverCall.close(status, metadata);
        return new ServerCall.Listener<>() {
            // noop
        };
    }
}