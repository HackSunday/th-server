import com.alibaba.fastjson.JSON;
import org.hacksunday.serve.business.comparing.HandValue;
import org.hacksunday.serve.business.comparing.HandValueFactory;
import org.hacksunday.serve.vo.Card;
import org.hacksunday.serve.vo.CardRankEnum;
import org.hacksunday.serve.vo.CardSuitEnum;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Test {

    public static void main(String[] args) {
        testStraightFlushHandValue();
        testStraightHandValue();
        testFullHouseHandValue();
    }

    private static void testStraightFlushHandValue() {
        List<Card> cardPool = new ArrayList<>();
        cardPool.add(new Card(CardSuitEnum.CLUBS, CardRankEnum.CARD_ACE));
        cardPool.add(new Card(CardSuitEnum.CLUBS, CardRankEnum.CARD_TWO));
        cardPool.add(new Card(CardSuitEnum.CLUBS, CardRankEnum.CARD_THREE));
        cardPool.add(new Card(CardSuitEnum.CLUBS, CardRankEnum.CARD_FOUR));
        cardPool.add(new Card(CardSuitEnum.CLUBS, CardRankEnum.CARD_FIVE));

        List<Card> hand1 = new ArrayList<>();
        hand1.add(new Card(CardSuitEnum.CLUBS, CardRankEnum.CARD_SIX));
        hand1.add(new Card(CardSuitEnum.CLUBS, CardRankEnum.CARD_SEVEN));
        List<Card> hand2 = new ArrayList<>();
        hand2.add(new Card(CardSuitEnum.DIAMONDS, CardRankEnum.CARD_SIX));
        hand2.add(new Card(CardSuitEnum.CLUBS, CardRankEnum.CARD_SEVEN));

        HandValue handValue1 = HandValueFactory.getHandValue(hand1, cardPool);
        HandValue handValue2 = HandValueFactory.getHandValue(hand2, cardPool);
        System.out.println(JSON.toJSONString(handValue1));
        System.out.println(JSON.toJSONString(handValue2));
        List<HandValue> handValues = new ArrayList<>();
        handValues.add(handValue1);
        handValues.add(handValue2);
        Collections.sort(handValues);
        System.out.println(JSON.toJSONString(handValues));
    }

    private static void testStraightHandValue() {
        List<Card> cardPool = new ArrayList<>();
        cardPool.add(new Card(CardSuitEnum.CLUBS, CardRankEnum.CARD_ACE));
        cardPool.add(new Card(CardSuitEnum.DIAMONDS, CardRankEnum.CARD_TWO));
        cardPool.add(new Card(CardSuitEnum.SPADES, CardRankEnum.CARD_THREE));
        cardPool.add(new Card(CardSuitEnum.HEARTS, CardRankEnum.CARD_KING));
        cardPool.add(new Card(CardSuitEnum.CLUBS, CardRankEnum.CARD_QUEEN));

        List<Card> hand1 = new ArrayList<>();
        hand1.add(new Card(CardSuitEnum.CLUBS, CardRankEnum.CARD_FOUR));
        hand1.add(new Card(CardSuitEnum.HEARTS, CardRankEnum.CARD_FIVE));
        List<Card> hand2 = new ArrayList<>();
        hand2.add(new Card(CardSuitEnum.DIAMONDS, CardRankEnum.CARD_JACK));
        hand2.add(new Card(CardSuitEnum.CLUBS, CardRankEnum.CARD_TEN));

        HandValue handValue1 = HandValueFactory.getHandValue(hand1, cardPool);
        HandValue handValue2 = HandValueFactory.getHandValue(hand2, cardPool);
        System.out.println(JSON.toJSONString(handValue1));
        System.out.println(JSON.toJSONString(handValue2));
        List<HandValue> handValues = new ArrayList<>();
        handValues.add(handValue1);
        handValues.add(handValue2);
        Collections.sort(handValues);
        System.out.println(JSON.toJSONString(handValues));
    }


    private static void testFullHouseHandValue() {
        List<Card> cardPool = new ArrayList<>();
        cardPool.add(new Card(CardSuitEnum.CLUBS, CardRankEnum.CARD_FIVE));
        cardPool.add(new Card(CardSuitEnum.DIAMONDS, CardRankEnum.CARD_TWO));
        cardPool.add(new Card(CardSuitEnum.SPADES, CardRankEnum.CARD_TWO));
        cardPool.add(new Card(CardSuitEnum.HEARTS, CardRankEnum.CARD_KING));
        cardPool.add(new Card(CardSuitEnum.CLUBS, CardRankEnum.CARD_KING));

        List<Card> hand1 = new ArrayList<>();
        hand1.add(new Card(CardSuitEnum.DIAMONDS, CardRankEnum.CARD_KING));
        hand1.add(new Card(CardSuitEnum.HEARTS, CardRankEnum.CARD_FIVE));
        List<Card> hand2 = new ArrayList<>();
        hand2.add(new Card(CardSuitEnum.SPADES, CardRankEnum.CARD_KING));
        hand2.add(new Card(CardSuitEnum.CLUBS, CardRankEnum.CARD_TWO));

        HandValue handValue1 = HandValueFactory.getHandValue(hand1, cardPool);
        HandValue handValue2 = HandValueFactory.getHandValue(hand2, cardPool);
        System.out.println(JSON.toJSONString(handValue1));
        System.out.println(JSON.toJSONString(handValue2));
        List<HandValue> handValues = new ArrayList<>();
        handValues.add(handValue1);
        handValues.add(handValue2);
        Collections.sort(handValues);
        System.out.println(JSON.toJSONString(handValues));
    }
}
