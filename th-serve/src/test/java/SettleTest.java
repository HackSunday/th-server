import com.alibaba.fastjson.JSON;
import org.hacksunday.serve.business.ChipsPool;
import org.hacksunday.serve.business.comparing.HandValue;
import org.hacksunday.serve.vo.HandValueRankEnum;
import org.hacksunday.serve.vo.PlayerSettlement;
import org.hacksunday.serve.vo.TexasPlayer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SettleTest {

    public static void main(String[] args) {
        int[] chips = {50, 250, 350, 5000, 5000, 500};
        Map<Integer, TexasPlayer> texasPlayerMap = new HashMap<>();
        for (int i = 0; i < 6; i++) {
            TexasPlayer texasPlayer = new TexasPlayer();
            texasPlayer.setUserId(i * 100 + 100);
            texasPlayer.setRemainChips(chips[i]);
            texasPlayerMap.put(i + 1, texasPlayer);
        }
        ChipsPool chipsPool = new ChipsPool(texasPlayerMap);

        chipsPool.allIn(1);
        chipsPool.allIn(2);
        chipsPool.allIn(3);
        chipsPool.putChips(4, 800);
        chipsPool.putChips(5, 800);
        chipsPool.allIn(6);


        Map<Integer, HandValue> posRankNumMap = new HashMap<>();
        posRankNumMap.put(1, new TestHandValue(HandValueRankEnum.HIGH_CARD, 100));
        posRankNumMap.put(2, new TestHandValue(HandValueRankEnum.HIGH_CARD, 100));
        posRankNumMap.put(4, new TestHandValue(HandValueRankEnum.HIGH_CARD, 50));
        posRankNumMap.put(5, new TestHandValue(HandValueRankEnum.HIGH_CARD, 30));
        posRankNumMap.put(3, new TestHandValue(HandValueRankEnum.HIGH_CARD, 10));
        List<PlayerSettlement> settle = chipsPool.settle(posRankNumMap);
        System.out.println(JSON.toJSONString(settle));
        System.out.println(JSON.toJSONString(texasPlayerMap));
    }
}
