import lombok.AllArgsConstructor;
import org.hacksunday.serve.business.comparing.HandValue;
import org.hacksunday.serve.vo.Card;
import org.hacksunday.serve.vo.HandValueRankEnum;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class TestHandValue implements HandValue {

    private HandValueRankEnum rankEnum;

    private int rankScore;

    @Override
    public HandValueRankEnum getRank() {
        return rankEnum;
    }

    @Override
    public List<Card> getCards() {
        return new ArrayList<>();
    }

    @Override
    public int getRankScore() {
        return rankScore;
    }
}
