FROM adoptopenjdk/openjdk11
EXPOSE 9091
ARG JAR_FILE=th-serve/target/th-server.jar
ADD ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]