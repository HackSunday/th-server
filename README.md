#Java德州扑克服务端
Java德州服务端，使用gRPC和客户端通信

gRPC协议可以使用 [bloomRPC](https://github.com/uw-labs/bloomrpc/releases) 来测试
## 连接指南
1. 基于jwt的用户认证
2. 客户端需连接房间和游戏，相关指令看参考

## 参考
### room相关指令
```json
// 准备
{
"command": 10
}
// 创建房间
{
"command": 0,
"body": "{\"smallBlind\":10,\"timeoutSeconds\":30,\"totalChairCount\":6}"
}
// 加入房间
{
"command": 1,
"body": "{\"roomId\":1}"
}
```

### 游戏相关指令
```json
// 连接服务器
{
"command": 80
}
// check
{
"command": 1
}
// call
{
"command": 2
}
// raise
{
"command": 3
"body": "{\"chips\":20}"
}
// fold
{
"command": 20
}

// all in
{
"command": 10
}
```